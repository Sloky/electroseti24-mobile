import axios from "axios";

// const host = 'https://mod3.energoline24.ru';
// const host = 'http://10.101.1.41';
// const host = 'http://10.101.1.10';
const host = 'http://192.168.1.112';

export const loginApiRequest = (username?: string, password?: string) => {
    return axios.post(`${host}/api/login`,
        {
            'username': username,
            'password': password
        },
    )
};

export const tasksApiRequest = (authToken: string|null) => {
    return axios.get(
        `${host}/api/task-to-work-list`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    )
};

export const uploadTasksApiRequest = (authToken: string|null, id: string, formData: any) => {
    return axios.post(
        `${host}/api/task/${id}/upload`,
        formData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
                'Content-Type': 'multipart/form-data',
            }
        },
    )
};

export const tasksProgressApiRequest = (authToken: string|null, id: string) => {
    return axios.get(
        `${host}/api/user/mobile-profile/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    )
}