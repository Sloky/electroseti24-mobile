import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        marginBottom: 10
    },
    image: {
        height: 150,
        // marginHorizontal: 10,
    },
    imageContainer: {
        marginBottom: 15,
        backgroundColor: THEME.BLACK_BACKGROUND,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconContainer: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
        borderRadius: 6,
    },
    spinner: {
        display: 'flex',
        alignItems: 'center',
        paddingBottom: 15,
    },
});