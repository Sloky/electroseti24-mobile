import React, {useState} from "react";
import {Alert, Dimensions, Image, Modal, TouchableOpacity, View} from "react-native";
import {Button, Spinner} from "@ui-kitten/components";
import {styles} from "./PhotoPickerStyles";
import {FontAwesome, MaterialCommunityIcons} from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Location from "expo-location";
import * as Permissions from 'expo-permissions';
import {THEME} from "../../theme";
import ImageViewer from "react-native-image-zoom-viewer";
import {Camera} from "expo-camera";
import * as MediaLibrary from 'expo-media-library';

interface PhotoPickerProps {
    onMakePhoto(photo: string): void
    buttonStyleStatus?: 'success' | 'basic' | 'control' | 'primary' | 'danger' | 'info' | 'warning'
    buttonAppearance?: 'filled' | 'ghost' | 'outline'
    buttonIconColor?: string
    withoutImage?: boolean
    isDisabled?: boolean
    isNeedLocation?: boolean
    buttonActiveOpacity?: number | undefined
    styleBtn?: object | undefined
    isClickable?: boolean | undefined
}

async function askForPermissions() {
    const {status} = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.MEDIA_LIBRARY,
    );
    if (status !== 'granted') {
        Alert.alert('Ошибка!', 'Вы не дали прав на создание фото');
        return false;
    }
    return true;
}

async function askForLocationPermissions() {
    const foregroundStatus = await Location.requestForegroundPermissionsAsync();
    // const backgroundStatus = await Location.requestBackgroundPermissionsAsync();
    if (foregroundStatus.status !== 'granted') {
        Alert.alert('Ошибка!', 'Вы не дали прав на использование местоположения');
        return false;
    }
    return true;
}

export function PhotoPicker(props: PhotoPickerProps) {
    const [image, setImage] = useState<string>();
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [showModal, setShowModal] = useState<boolean>(false);

    const withoutImage: boolean = !!props.withoutImage;

    const isDisabled: boolean = !!props.isDisabled;

    const isNeedLocation: boolean = !!props.isNeedLocation;

    const screenSize = Dimensions.get('screen');

    const takePhoto = async () => {

        if (isNeedLocation) {
            let locIsEnabled = await Location.hasServicesEnabledAsync();
            if (!locIsEnabled) {
                Alert.alert('Пожалуйста включите геолокацию');
                return;
            } else {
                let locationPermissions = await askForLocationPermissions();
                if (!locationPermissions) {
                    return;
                }
            }
        }

        const hasPermissions = await askForPermissions();

        if (!hasPermissions) {
            setSubmitting(false);
            return;
        }

        const img = await ImagePicker.launchCameraAsync({
            quality: 0.8,
            allowsEditing: false,
            aspect: [16, 9]
        });

        setSubmitting(true);

        if (!(img.cancelled)) {
            setImage(img.uri)
            props.onMakePhoto(img.uri);
        }
        setSubmitting(false);
    };

    const photoIcon = () => (
        <MaterialCommunityIcons name="camera-plus-outline" size={24} color={isDisabled ? THEME.INACTIVE_TEXT : props.buttonIconColor || "white"}/>
    );

    return (
        <View style={styles.wrapper}>
            {submitting ? (
                <View style={styles.spinner}>
                    <Spinner size={'giant'}/>
                </View>
            ) : <View/>}
            {(image && !withoutImage) &&
            (props.isClickable) &&
            <>
                <TouchableOpacity
                    style={{...styles.imageContainer, width: screenSize.width * 0.4}}
                    onPress={() => setShowModal(true)}
                >
                    <Image
                        resizeMode={'contain'}
                        style={{...styles.image, width: screenSize.width * 0.4}}
                        source={{uri: image}}
                    />
                    <View style={styles.iconContainer}>
                        <FontAwesome name="search-plus" size={34} color={THEME.BACKGROUND_WITH_OPACITY}/>
                    </View>
                </TouchableOpacity>
                <Modal
                    visible={showModal}
                    transparent={true}
                    onRequestClose={() => setShowModal(false)}
                >
                    <ImageViewer
                        onClick={() => {}}
                        enableSwipeDown={true}
                        onSwipeDown={() => setShowModal(false)}
                        imageUrls={[{url: image}]}
                    />
                </Modal>
            </>
            }
            {(image && !withoutImage) &&
            (!props.isClickable) &&
            <View style={{...styles.imageContainer, width: screenSize.width * 0.4}}>
                <Image
                    resizeMode={'contain'}
                    style={{...styles.image, width: screenSize.width * 0.4}}
                    source={{uri: image}}
                />
            </View>
            }
            <Button
                onPress={takePhoto}
                status={props.buttonStyleStatus || 'warning'}
                appearance={props.buttonAppearance || 'filled'}
                accessoryLeft={photoIcon}
                disabled={isDisabled}
                activeOpacity={props.buttonActiveOpacity}
                style={props.styleBtn}
                size={'medium'}
            >

                Сделать фото
            </Button>
        </View>
    );
}