import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    loaderContainer: {
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        alignItems: "center",
    },
});