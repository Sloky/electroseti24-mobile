import React from "react";
import {ActivityIndicator, View, Dimensions} from "react-native";
import {THEME} from "../../theme";
import {styles} from "./LoaderComponentStyles";

export function LoaderComponent() {
    const screen = Dimensions.get('screen');

    return (
        <View style={{...styles.loaderContainer, height: screen.height, width: screen.width}}>
            <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
        </View>
    );
}
