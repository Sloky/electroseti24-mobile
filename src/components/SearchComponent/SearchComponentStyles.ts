import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    searchContainer: {
        position: 'absolute',
        // bottom: '3%',
        left: '0%',
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
    searchIcon: {
        position: 'absolute',
        bottom: '3%',
        left: '5%',
        zIndex: 1,
        width: 45,
        height: 45,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchWrapper: {
        flex: 1,
        height: 45,
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 50,
        borderColor: THEME.PRIMARY_COLOR,
        backgroundColor: THEME.BACKGROUND_COLOR,
        paddingRight: 10,
    },
    searchInputIconView: {
        justifyContent: 'center',
    },
    searchField: {
        flex: 1,
        height: 40,
        paddingLeft: 50,
    },

});
