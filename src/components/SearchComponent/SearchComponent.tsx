import React, {Dispatch, MutableRefObject, SetStateAction, useState} from "react";
import {Alert, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {THEME} from "../../theme";
import {EvilIcons, Ionicons, MaterialIcons, MaterialCommunityIcons, AntDesign} from "@expo/vector-icons";
import {useDispatch, useSelector} from "react-redux";
import {useNavigation} from "@react-navigation/core";
import {globalStateInterface} from "../../store/combinedState";
import {AppAction} from "../../store/app/actions";
import {styles} from './SearchComponentStyles';

interface SearchProps {
    onChangeSearchValue?: any,
    searchPosition?: number
}

export default function SearchComponent(props: SearchProps) {
    const [search, setSearch] = useState<boolean>(false);
    const [searchValue, setSearchValue] = useState<string>('');

    const navigation = useNavigation();
    const dispatch = useDispatch();

    const userData = useSelector((state: globalStateInterface) => state.app.user);

    const showHideSearchInput = () => {
        setSearch((prev): boolean => {
            return !prev;
        });
    };

    const onChangeSearchValue = (value: string) => {
        setSearchValue(value);
        props.onChangeSearchValue(value);
    };

    return (
        <>
            <TouchableOpacity style={{
                ...styles.searchIcon,
                backgroundColor: search ? THEME.SECONDARY_COLOR : THEME.SECONDARY_WITH_OPACITY,
                bottom: props.searchPosition ? `${props.searchPosition}%` : '3%',
            }} onPress={showHideSearchInput}>
                <Ionicons name="search" size={28} color={search ? THEME.BACKGROUND_COLOR : THEME.BACKGROUND_WITH_OPACITY}/>
            </TouchableOpacity>
            {search &&
            <View style={{...styles.searchContainer, bottom: props.searchPosition ? `${props.searchPosition}%` : '3%',}}>
                <View style={{...styles.searchWrapper}}>
                    <TextInput
                        style={{...styles.searchField}}
                        focusable={search}
                        value={searchValue}
                        onChangeText={(value: string) => onChangeSearchValue(value)}
                        autoFocus={true}
                    />
                    <TouchableOpacity style={styles.searchInputIconView} onPress={() => onChangeSearchValue('')}>
                        <AntDesign name="close" size={28} color={THEME.SECONDARY_COLOR}/>
                    </TouchableOpacity>
                </View>
            </View>}
        </>
    );
}