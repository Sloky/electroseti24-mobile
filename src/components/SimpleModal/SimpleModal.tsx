import React, {Component, FunctionComponent, ReactComponentElement, useEffect, useState} from "react";
import {Text, View, Modal, TouchableWithoutFeedback} from "react-native";
import {Button} from "@ui-kitten/components";
import {styles} from "./SimpleModalStyles";
import {AntDesign} from "@expo/vector-icons";
import {THEME} from "../../theme";

interface SimpleModalProps {
    isVisible: boolean

    onCloseModal(): void

    children?: ReactComponentElement<any>
}

export default function SimpleModal(props: SimpleModalProps) {

    const onCloseModal = () => {
        props.onCloseModal()
    }

    const closeModalIcon = () => (
        <AntDesign name="close" size={24} color={THEME.SECONDARY_COLOR}/>
    )

    return (
        <View>
            {/*<Modal*/}
            {/*    style={styles.modal}*/}
            {/*    visible={props.isVisible}*/}
            {/*    backdropStyle={styles.backdrop}*/}
            {/*    onBackdropPress={onCloseModal}*/}
            {/*>*/}
            {/*    {props.children}*/}
            {/*</Modal>*/}

            <Modal
                animationType={'slide'}
                transparent={true}
                visible={props.isVisible}
                onRequestClose={onCloseModal}
            >
                <TouchableWithoutFeedback onPress={onCloseModal}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalContent}>
                            <Button style={styles.closeBtn} activeOpacity={0.7} appearance={"ghost"} accessoryLeft={closeModalIcon} onPress={onCloseModal}/>
                            {props.children}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>
    );
}