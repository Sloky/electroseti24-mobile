import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

// export const styles = StyleSheet.create({
//     backdrop: {
//         backgroundColor: 'rgba(0, 0, 0, 0.5)',
//     },
// });

export const styles = StyleSheet.create({
    centeredView: {
        // backgroundColor: NEW_THEME.LOADER_BACKGROUND_COLOR,
        flex: 1,
        justifyContent: "flex-end",
    },
    modalContent: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        borderWidth: 1,
        borderColor: THEME.OUTLINE_COLOR,
    },
    closeBtn: {
        flex: 0.1,
        position: "absolute",
        top: 0,
        right: 0,
    },
});