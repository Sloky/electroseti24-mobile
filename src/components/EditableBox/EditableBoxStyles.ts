import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        borderBottomWidth: 1,
        borderBottomColor: THEME.OUTLINE_COLOR,
        marginBottom: 10,
        paddingBottom: 10,
        width: '100%',
    },
    content: {
        paddingHorizontal: 20,
    },
    imageContainer: {
        margin: 5,
        backgroundColor: THEME.BLACK_BACKGROUND,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        height: 150,
        marginHorizontal: 10,
    },
    iconContainer: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
        borderRadius: 6,
    },
    boxItem: {
        flexDirection: 'row',
        paddingVertical: 5,
    },
    cardItemTitle: {
        fontSize: 15,
        fontWeight: '700',
        color: THEME.TEXT_COLOR,
        marginRight: 10,
    },
    cardItemValue: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
});