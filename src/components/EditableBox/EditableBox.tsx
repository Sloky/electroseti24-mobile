import React, {ReactComponentElement, useState} from "react";
import {Dimensions, Image, Modal, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./EditableBoxStyles";
import {THEME} from "../../theme";
import {FontAwesome} from "@expo/vector-icons";
import ImageViewer from "react-native-image-zoom-viewer";

interface EditableBoxProps {
    items: {
        title: string
        value: string
    }[],
    modal?: ReactComponentElement<any>
    images?: string[]
}

const defaultProps: EditableBoxProps = {
    items: [],
    modal: <></>
}

export default function EditableBox(props: EditableBoxProps) {
    const [showModal, setShowModal] = useState<boolean>(false);

    const screenSize = Dimensions.get('screen');

    return (
        <>
            {props.items.length && (
                <View style={styles.wrapper}>
                    <View style={styles.content}>
                        {props.items.map(item => (
                            <View style={styles.boxItem} key={item.title}>
                                <Text style={styles.cardItemTitle}>{item.title} </Text>
                                <View style={{flex: 1,}}>
                                    <Text style={styles.cardItemValue}>{item.value}</Text>
                                </View>
                            </View>
                        ))}
                        {props.images?.length && (
                            <>
                                <TouchableOpacity
                                    style={{...styles.imageContainer, width: screenSize.width * 0.4}}
                                    onPress={() => setShowModal(true)}
                                >
                                    <Image
                                        resizeMode={'contain'}
                                        style={{...styles.image, width: screenSize.width * 0.4}}
                                        source={{uri: props.images[props.images.length - 1]}}
                                    />
                                    <View style={styles.iconContainer}>
                                        <FontAwesome name="search-plus" size={34} color={THEME.BACKGROUND_WITH_OPACITY}/>
                                    </View>
                                </TouchableOpacity>
                                <Modal
                                    visible={showModal}
                                    transparent={true}
                                    onRequestClose={() => setShowModal(false)}
                                >
                                    <ImageViewer
                                        onClick={() => {}}
                                        enableSwipeDown={true}
                                        onSwipeDown={() => setShowModal(false)}
                                        imageUrls={[{url: props.images[props.images.length - 1]}]}
                                    />
                                </Modal>
                            </>
                        )}
                        {props.modal}
                    </View>
                </View>
            )}
        </>
    );
}

EditableBox.defaultProps = defaultProps;