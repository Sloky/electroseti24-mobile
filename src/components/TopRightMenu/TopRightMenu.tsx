import React, {useState} from "react";
import {TouchableOpacity, StyleSheet} from "react-native";
import {Ionicons} from "@expo/vector-icons";
import {THEME} from "../../theme";
import {useNavigation} from "@react-navigation/core";
import {DrawerActions} from "@react-navigation/native";
import {MenuItem, OverflowMenu} from "@ui-kitten/components";

interface TopRightMenuProps {
    items: {
        title: string
        link: string
    }[]
    // onMenuPress(): void
}

export function TopRightMenu(props: TopRightMenuProps) {
    const [visible, setVisible] = useState<boolean>(false);

    const navigation = useNavigation();

    const onItemPress = (link: string) => {
        setVisible(false);
        navigation.navigate(link);
    };

    return (
        <OverflowMenu
            anchor={() =>
                <TouchableOpacity
                    onPress={() => setVisible(true)}
                    style={styles.button}
                >
                    <Ionicons name="menu" size={24} color={THEME.BACKGROUND_COLOR} />
                </TouchableOpacity>
            }
            style={{width: '90%', marginRight: -10}}
            visible={visible}
            backdropStyle={styles.backdrop}
            placement={'bottom start'}
            onBackdropPress={() => setVisible(false)}
        >
            {props.items.map((item, index) => (
                <MenuItem
                    onPress={() => onItemPress(item.link)}
                    title={item.title}
                    key={index}
                    style={{width: 500}}
                />
            ))}
        </OverflowMenu>
    );
}

const styles = StyleSheet.create({

    button: {
        marginRight: 10
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});