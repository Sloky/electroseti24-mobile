import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
    },
    buttonGroups: {
        flexDirection: 'row',
        height: 60,
        borderWidth: 1,
        borderColor: THEME.OUTLINE_COLOR,
    },
    footerButton: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    icons: {
        display: 'flex',
        textAlign: 'center'
    },
    text: {
        textAlign: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    btnText: {
        textAlign: "center",
        fontSize: 13,
    },
});