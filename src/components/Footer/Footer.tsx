import React, {MutableRefObject} from "react";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import {THEME} from "../../theme";
import {MaterialIcons, MaterialCommunityIcons} from "@expo/vector-icons";
import {useDispatch, useSelector} from "react-redux";
import {useNavigation} from "@react-navigation/core";
import {globalStateInterface} from "../../store/combinedState";
import {AppAction} from "../../store/app/actions";
import {styles} from './FooterStyles';


interface FooterProps {
    activeBtn: string
    saveAlert?: MutableRefObject<boolean>
}

export default function Footer(props: FooterProps) {

    const navigation = useNavigation();
    const dispatch = useDispatch();

    const userData = useSelector((state: globalStateInterface) => state.app.user);

    const exitAlert = (screen: string) => {
        if (props.saveAlert && props.saveAlert.current) {
            Alert.alert(
                'Все несохраненные данные будут утеряны.',
                'Перейти?',
                [
                    {text: 'Да', onPress: () => {
                            navigation.navigate(screen);
                        }},
                    {text: 'Нет', onPress: () => {}},
                ]
            );
        } else {
            navigation.navigate(screen);
        }
    };

    const onProfilePress = () => {
        exitAlert('ProfileScreen');
    };

    const onCameraPress = () => {
        exitAlert('MainsObjectScreen');
    };

    const onTaskPress = () => {
        exitAlert('AddressListScreen');
    };

    return (
        <View style={styles.wrapper}>
            <View style={styles.buttonGroups}>
                <TouchableOpacity style={styles.footerButton} onPress={onProfilePress}>
                    <MaterialIcons name="person-outline" size={26} color={props.activeBtn === 'Profile' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR} style={styles.icons}/>
                    <Text style={{
                        ...styles.btnText,
                        fontWeight: props.activeBtn === 'Profile' ? '700' : '400',
                        color: props.activeBtn === 'Profile' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR,
                    }}>Профиль</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.footerButton} onPress={onCameraPress}>
                    <MaterialCommunityIcons name="camera-outline" size={26} color={props.activeBtn === 'Photo' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR} style={styles.icons}/>
                    <Text style={{
                        ...styles.btnText,
                        fontWeight: props.activeBtn === 'Photo' ? '700' : '400',
                        color: props.activeBtn === 'Photo' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR,
                    }}>Фотофиксация</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.footerButton} onPress={onTaskPress}>
                    <MaterialCommunityIcons name="clipboard-text-outline" size={26} color={props.activeBtn === 'Tasks' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR} style={styles.icons}/>
                    <Text style={{
                        ...styles.btnText,
                        fontWeight: props.activeBtn === 'Tasks' ? '700' : '400',
                        color: props.activeBtn === 'Tasks' ? THEME.PRIMARY_COLOR : THEME.SECONDARY_COLOR,
                    }}>Задачи</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.text}>{(userData && userData.surname) + ' ' + (userData && userData.name) + ' ' + (userData && userData.patronymic)}</Text>
        </View>
    );
}