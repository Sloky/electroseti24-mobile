import React from 'react';
import {Text, View, StyleSheet} from "react-native";
import {useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";

export function ControllerName() {
    const userData = useSelector((state: globalStateInterface) => state.app.user);

    return (
        <Text style={styles.text}>{(userData && userData.surname) + ' ' + (userData && userData.name) + ' ' + (userData && userData.patronymic)}</Text>
    )
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
    }
})