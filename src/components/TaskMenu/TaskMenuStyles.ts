import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    btnGroup: {
        flexDirection: 'row',
        borderWidth: 2,
        borderColor: THEME.OUTLINE_COLOR,
        borderRadius: 7,
    },
    btn: {
        padding: 10,
        flex: 0.5,
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderWidth: 0,
        textAlign: "center",
        borderRadius: 6,
    },
    activeBtn: {
        padding: 10,
        flex: 0.5,
        borderWidth: 2,
        borderRadius: 6,
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderColor: THEME.PRIMARY_COLOR,
    },
    activeText: {
        textAlign: 'center',
        color: THEME.PRIMARY_COLOR,
        fontSize: 16,
        fontWeight: '700',
    },
    text: {
        textAlign: 'center',
        color: THEME.SECONDARY_COLOR,
        fontSize: 16,
    },
});