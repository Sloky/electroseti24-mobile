import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import {Button} from "@ui-kitten/components";
import {useNavigation} from "@react-navigation/core";
import {styles} from './TaskMenuStyles';
import {FalsyText} from "@ui-kitten/components/devsupport";

interface TaskMenuProps {
    button1: {
        title: string,
        link: string,
        appearance?: string
        active?: boolean
    },
    button2: {
        title: string,
        link: string,
        appearance?: string
        active?: boolean
    },
}

export function TaskMenu(props: TaskMenuProps) {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.btnGroup}>
                {/*<Button*/}
                {/*    // appearance={props.button1.appearance}*/}
                {/*    appearance={'ghost'}*/}
                {/*    style={props.button1.active ? styles.activeBtn : styles.btn}*/}
                {/*    onPress={() => navigation.navigate(props.button1.link)}*/}
                {/*>*/}
                {/*    {props.button1.title}*/}
                {/*</Button>*/}
                {/*<Button*/}
                {/*    // appearance={props.button2.appearance}*/}
                {/*    appearance={'ghost'}*/}
                {/*    style={props.button2.active ? styles.activeBtn : styles.btn}*/}
                {/*    onPress={() => navigation.navigate(props.button2.link)}*/}
                {/*>*/}
                {/*    {props.button2.title}*/}
                {/*</Button>*/}
                <TouchableOpacity
                    onPress={() => navigation.navigate(props.button1.link)}
                    style={props.button1.active ? styles.activeBtn : styles.btn}
                >
                    <Text style={props.button1.active ? styles.activeText : styles.text}>{props.button1.title}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate(props.button2.link)}
                    style={props.button2.active ? styles.activeBtn : styles.btn}
                >
                    <Text style={props.button2.active ? styles.activeText : styles.text}>{props.button2.title}</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}