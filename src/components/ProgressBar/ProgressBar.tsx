import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {THEME} from "../../theme";

interface ProgressBarProps {
    progress: number,
    text?: string
}

export function ProgressBar(props: ProgressBarProps) {
    return (
        <View style={styles.wrapper}>
            <View style={styles.progressContainer}>
                <View style={{...styles.progressBand, width: `${props.progress}%`}}/>
            </View>
            {props.text && <Text style={styles.text}>{props.text}</Text>}
        </View>
    );
}

export const styles = StyleSheet.create({
    wrapper: {
        // borderColor: 'black',
        // borderWidth: 2,
        // height: 70,
        marginBottom: 5,
        paddingHorizontal: 10,
        alignItems: 'center'
    },
    progressContainer: {
        borderColor: THEME.PRIMARY_COLOR,
        borderRadius: 10,
        borderWidth: 1,
        height: 10,
        overflow: 'hidden',
        width: '100%',
    },
    progressBand: {
        backgroundColor: THEME.PRIMARY_COLOR,
        // borderRadius: 10,
        height: 28,
        width: 0
    },
    text: {
        color: THEME.PRIMARY_COLOR,
        fontSize: 18
    }
});