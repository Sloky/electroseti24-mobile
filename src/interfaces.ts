export interface IDictionary<TValue> {
    [id: string] : TValue
}

export interface UserData {
    id: string,
    address: string,
    company: CompanyData,
    groups: UserGroupData[],
    name: string,
    patronymic: string,
    phone: string,
    roles: string[],
    surname: string,
    username: string,
    work_status: number,
    password?: string,
}

export interface UserGroupData {
    id: string,
    description?: string,
    group_name?: string
    roles?: string[]
}

export interface CompanyData {
    id: string,
    parent: CompanyData|null
    children: CompanyData[]|[]
    title: string
}

interface FeederData {
    id: string
    title: string
    substation: SubstationData,
    transformer: TransformerData
}

interface SubstationData {
    id: string
    title: string
}

interface TransformerData {
    id: string
    title: string
    feeder: FeederData
}

interface CounterTypeData {
    "title": string
    "electron_current_type": number
    "number_phases": number
    "number_tariffs": number|null
    "tariff_type": string
    "operating_mechanism": number|null
    "class_accuracy": number
    "indicator_device": number|null
    "capacity": number|null
}

interface FailureFactorData {
    id: string
    title: string
    message: string
}

export interface TaskData {
    id: string
    task_type: number
    executor: UserData|null
    priority: number
    deadline: string
    status: number
    coordinate_x: number|null
    coordinate_y: number|null
    operator_comment: string|null
    controller_comment: string|null
    failure_factors: FailureFactorData[]
    actualized_fields: string[]
    changed_fields: string[]
    company: CompanyData
    locality: string
    street: string
    house_number: string
    porch_number: string
    apartment_number: string
    lodgers_count: number|undefined
    rooms_count: number|undefined
    subscriber_type: number
    subscriber_title: string
    subscriber_name: string
    subscriber_surname: string
    subscriber_patronymic: string
    subscriber_phones: string[]
    agreement_number: string
    counter_number: string
    last_counter_value: number[]
    current_counter_value: number[]|null
    consumption: number|null
    consumption_coefficient: number
    counter_physical_status: number
    substation: SubstationData
    feeder: FeederData
    transformer: TransformerData
    line_number: string
    electric_pole_number: string
    counter_type: CounterTypeData
    terminal_seal: string
    anti_magnetic_seal: string
    side_seal: string
    photos: string[]
    execution_date_time?: string
    newCoordinates?: CoordinateInterface
    temporary_address: string
    temporary_counter_type?: string
    temporary_substation?: string
    temporary_transformer?: string
    temporary_feeder?: string
    period?: string
}

export interface TasksCollection {
    [id: string]: TaskData
}

export interface AddressListItemInterface {
    address: string
    counterQuantity: number
    identifiers: string[]
}

export interface CoordinateInterface {
    GPSLatitude: number
    GPSLongitude: number
}

export interface TaskUploadInterface {
    id: string
    status: number
    executedDateTime: string

    newCoords?: CoordinateInterface

    counterPhysicalStatus: number

    counterPhotos?: string[]

    counterValue: number|null
    counterValuePhotos?: string[]

    counterNumber?: string

    counterTypeString?: string

    terminalSeal?: string
    terminalSealPhotos?: string[]

    antimagneticSeal?: string
    antimagneticSealPhotos?: string[]

    sideSeal?: string
    sideSealPhotos?: string[]

    lodgersCount?: number
    roomsCount?: number

    controllerComment?: string
}