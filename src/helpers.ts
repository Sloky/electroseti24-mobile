import {AddressListItemInterface, IDictionary, TaskData, TasksCollection, TaskUploadInterface} from "./interfaces";
import * as FileSystem from "expo-file-system";
import {MainsObjectCollection} from "./store/mainsObect/state";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";

export function getFullAddress(task: TaskData, forTable: boolean = false) {
    let address = task.temporary_address || 'Нет данных';
    if (task.locality && task.street) {
        address = task.locality + ', ' + task.street;
        if (task.house_number) {
            address = task.locality + ', ' + task.street + ', дом ' + task.house_number;
            if (task.apartment_number && !forTable) {
                address = task.locality + ', ' + task.street + ', дом ' + task.house_number + ', кв. ' + task.apartment_number;
            }
        }
    }
    return address;
}

export function getAddressList(tasks: TaskData[]): [] {
    let addressArray: any = [];
    if (tasks.length !== 0) {
        let addressList: IDictionary<AddressListItemInterface> = {};
        tasks.map(value => {
            let address = getFullAddress(value, true);
            if (addressList.hasOwnProperty(address)) {
                addressList[address].counterQuantity += 1;
                addressList[address].identifiers.push(value.id);
            } else {
                addressList[address] = {address: address, counterQuantity: 1, identifiers: [value.id]};
            }
        });
        for (let key in addressList) {
            addressArray.push(addressList[key]);
        }
    }
    return addressArray;
}

export function getTasksCollection(tasks: TaskData[]) {
    let tasksCollection: TasksCollection = {};
    tasks.map(task => {
        tasksCollection[task.id] = task;
    });
    return tasksCollection;
}

export function getObjCompDifField<TValue>(obj1: TValue, obj2: TValue): string[] {
    let difFields: string[] = [];
    Object.keys(obj1).map(key => {
        // @ts-ignore
        if (obj1[key] !== obj2[key]) {
            difFields.push(key);
        }
    });
    return difFields;
}

export function getImagesByPrefix(photos: string[], prefix: string): string[]|undefined {
    let regexp = new RegExp(prefix);
    let filteredPhotos = photos.filter(photo => regexp.test(photo));
    return filteredPhotos.length > 0 ? filteredPhotos : undefined;
}

export function deleteTaskFromState(tasks: TasksCollection|undefined, taskId: string): TasksCollection|undefined {
    if (tasks) {
        let newTasks = {...tasks};
        delete newTasks[taskId];
        return newTasks;
    }
    return undefined;
}

export function deleteMainsObjectFromState(mainsObjects: MainsObjectCollection|undefined, mainsObjectId: string): MainsObjectCollection|undefined {
    if (mainsObjects) {
        let newTasks = {...mainsObjects};
        delete newTasks[mainsObjectId];
        return newTasks;
    }
    return undefined;
}

export async function removeAllTheFiles() {
    const docDirUri = FileSystem.documentDirectory
        || 'file:///data/user/0/host.exp.exponent/files/ExperienceData/%2540fimkos%252Fschet_uchet_mobile_v0_1_0/';
    try {
        await FileSystem.readDirectoryAsync(docDirUri).then((photos: string[]) => {
            photos.map((photoName: string) => {
                let fileName = docDirUri + photoName;
                FileSystem.deleteAsync(fileName, {idempotent: true});
            });
        }).catch(reason => {
            console.log('Fail get the photos: ', reason);
        });
    } catch (e) {
        console.log(e);
    }
}

export async function removePhotos(photos: string[]) {
    try {
        photos.map((photo) => {
            FileSystem.deleteAsync(photo, {idempotent: true});
        });
    } catch (e) {
        console.log('removePhotos', e);
    }
}

export async function removeMainsObjectFiles() {
    const docDirUri = FileSystem.documentDirectory
        || 'file:///data/user/0/host.exp.exponent/files/ExperienceData/%2540fimkos%252Fschet_uchet_mobile_v0_1_0/';
    try {
        await FileSystem.readDirectoryAsync(docDirUri).then((photos: string[]) => {
            let reqExp = new RegExp('mains_object_', 'i');
            let mainsObjectPhotos = photos.filter(photo => reqExp.test(photo));
            mainsObjectPhotos.map((photoName: string) => {
                let fileName = docDirUri + photoName;
                FileSystem.deleteAsync(fileName, {idempotent: true});
            });
        }).catch(reason => {
            console.log('Fail get the photos: ', reason);
        });
    } catch (e) {
        console.log(e);
    }
}

export async function removeMainsObjectFile(fileUri: string) {
    try {
        await FileSystem.deleteAsync(fileUri, {idempotent: false});
    } catch (e) {
        console.log(e);
    }
}

export function getTaskUploadData(task: TaskData): TaskUploadInterface {
    let uploadData: TaskUploadInterface = {
        id: task.id,
        // status: task.status,
        status: 1,
        counterPhysicalStatus: task.counter_physical_status,
        counterValue: task.current_counter_value ? task.current_counter_value[0] : null,
        controllerComment: task.controller_comment || undefined,
        counterValuePhotos: getImagesByPrefix(task.photos, 'counter_value_photo'),
        executedDateTime: task.execution_date_time || '',
        newCoords: task.newCoordinates
    };

    if (task.changed_fields.length > 0) {
        for(let field in task.changed_fields) {
            switch (task.changed_fields[field]) {
                case 'counter_number' :
                    uploadData.counterNumber = task.counter_number;
                    uploadData.counterPhotos = getImagesByPrefix(task.photos, 'counter_photo');
                    break;
                case 'counter_physical_status' :
                    uploadData.counterPhysicalStatus = task.counter_physical_status;
                    uploadData.counterPhotos = getImagesByPrefix(task.photos, 'counter_photo');
                    break;
                case 'counter_type_title':
                    uploadData.counterTypeString = task.counter_type.title;
                    uploadData.counterPhotos = getImagesByPrefix(task.photos, 'counter_photo')
                    break;
                case 'terminal_seal' :
                    uploadData.terminalSeal = task.terminal_seal;
                    uploadData.terminalSealPhotos = getImagesByPrefix(task.photos, 'terminal_seal_photo');
                    break;
                case 'anti_magnetic_seal' :
                    uploadData.antimagneticSeal = task.anti_magnetic_seal;
                    uploadData.antimagneticSealPhotos = getImagesByPrefix(task.photos, 'anti_magnetic_seal_photo');
                    break;
                case 'side_seal' :
                    uploadData['sideSeal'] = task.side_seal;
                    uploadData['sideSealPhotos'] = getImagesByPrefix(task.photos, 'side_seal_photo');
                    break;
                case 'rooms_count' :
                    uploadData.roomsCount = task.rooms_count;
                    break;
                case 'lodgers_count' :
                    uploadData.lodgersCount = task.lodgers_count;
                    break;
            }
        }
    }

    return uploadData;
}

export function getTaskUploadFormData(taskData: TaskUploadInterface) {
    let formData = new FormData();
    formData.append('taskId', taskData.id);
    formData.append('status', taskData.status.toString());
    formData.append('counterPhysicalStatus', taskData.counterPhysicalStatus.toString());
    formData.append('executedDateTime', taskData.executedDateTime);

    if (taskData.counterValue) {
        formData.append('counterValue', taskData.counterValue.toString());
    }

    if (taskData.counterValuePhotos) {
        formData.append('counterValuePhoto', {
            type: 'image/jpg',
            // @ts-ignore
            uri: taskData.counterValuePhotos[taskData.counterValuePhotos.length - 1],
            name: 'counter_value_photo_'+taskData.id
        });
    }

    if (taskData.counterPhotos) {
        formData.append('counterPhoto', {
            type: 'image/jpg',
            // @ts-ignore
            uri: taskData.counterPhotos[taskData.counterPhotos.length - 1],
            name: 'counter_photo_'+taskData.id
        });
    }

    if (taskData.counterNumber) {
        formData.append('counterNumber', taskData.counterNumber);
    }

    if (taskData.counterTypeString) {
        formData.append('counterTypeString', taskData.counterTypeString);
    }

    if (taskData.terminalSeal) {
        formData.append('terminalSeal', taskData.terminalSeal);
    }

    if (taskData.terminalSealPhotos) {
        formData.append('terminalSealPhoto', {
            type: 'image/jpg',
            // @ts-ignore
            uri: taskData.terminalSealPhotos[taskData.terminalSealPhotos.length - 1],
            name: 'terminal_seal_photo_'+taskData.id
        });
    }

    if (taskData.antimagneticSeal) {
        formData.append('antimagneticSeal', taskData.antimagneticSeal);
    }

    if (taskData.antimagneticSealPhotos) {
        formData.append('antimagneticSealPhoto', {
            type: 'image/jpg',
            // @ts-ignore
            uri: taskData.antimagneticSealPhotos[taskData.antimagneticSealPhotos.length - 1],
            name: 'antimagnetic_seal_photo_'+taskData.id
        });
    }

    if (taskData.sideSeal) {
        formData.append('sideSeal', taskData.sideSeal);
    }

    if (taskData.sideSealPhotos) {
        formData.append('sideSealPhoto', {
            type: 'image/jpg',
            // @ts-ignore
            uri: taskData.sideSealPhotos[taskData.sideSealPhotos.length - 1],
            name: 'side_seal_photo_'+taskData.id
        });
    }

    if (taskData.lodgersCount) {
        formData.append('lodgersCount', taskData.lodgersCount.toString());
    }

    if (taskData.roomsCount) {
        formData.append('roomsCount', taskData.roomsCount.toString());
    }

    if (taskData.controllerComment) {
        formData.append('controllerComment', taskData.controllerComment);
    }

    if (taskData.newCoords) {
        formData.append('GPSLongitude', taskData.newCoords.GPSLongitude.toString());
        formData.append('GPSLatitude', taskData.newCoords.GPSLatitude.toString());
    }

    return formData;
}

export function changeFloatingPoint(str: string) {
    return  str.replace(',', '.');
}

export const getUnchangeableTasks = (tasks: TaskData[]): string[]|[] => {
    let unchangeableTasks = tasks.filter(task => {
        return (task.status == 1) || (task.changed_fields.length > 0);
    });
    let IDs: string[] = [];
    if (unchangeableTasks.length > 0) {
        IDs = unchangeableTasks.map(task => task.id);
    }
    return IDs;
}

export async function getCurrentPosition() {
    let location: any;
    location = await Location.getCurrentPositionAsync()
        .catch(e => console.log('empty', e));
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.BestForNavigation})
            .catch(e => console.log('BestForNavigation', e));
    }
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.Highest})
            .catch(e => console.log('Highest', e));
    }
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.High})
            .catch(e => console.log('High', e));
    }
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.Balanced})
            .catch(e => console.log('Balanced', e));
    }
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.Low})
            .catch(e => console.log('Low', e));
    }
    if (!location) {
        location = await Location.getCurrentPositionAsync({accuracy: LocationAccuracy.Lowest})
            .catch(e => console.log('Lowest', e));
    }
    if (!location) {
        location = await Location.getLastKnownPositionAsync()
            .catch(e => console.log('Last', e));
    }
    console.log('LOCATION', location);
    return location;
}