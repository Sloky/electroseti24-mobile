import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {LoginScreen} from "../screens/LoginScreen/LoginScreen";
import {AddressListScreen} from "../screens/AddressListScreen/AddressListScreen";
import {AgreementListScreen} from "../screens/AgreementListScreen/AgreementListScreen";
import {useSelector} from "react-redux";
import {AgreementScreen} from "../screens/AgreementScreen/AgreementScreen";
import {globalStateInterface} from "../store/combinedState";
import MainScreen from "../screens/MainScreen/MainScreen";
import {TaskScreen} from "../screens/TaskScreen/TaskScreen";
import {SubscriberScreen} from "../screens/SubscriberScreen/SubscriberScreen";
import {ExecutedTaskListScreen} from "../screens/ExecutedTaskListScreen/ExecutedTaskListScreen";
import {MainsObjectScreen} from "../screens/MainsObjectScreen/MainsObjectScreen";
import {MaterialCommunityIcons, MaterialIcons} from "@expo/vector-icons";
import {createNativeStackNavigator} from "react-native-screens/native-stack";
import {ProfileScreen} from "../screens/ProfileScreen/ProfileScreen";
import {LoaderComponent} from "../components/LoaderComponent/LoaderComponent";

const Stack = createStackNavigator();

function AppStack() {
    let state = useSelector((state: globalStateInterface) => state);
    const loading = useSelector((state: globalStateInterface) => state.app.loading);

    return (
        <>
            <Stack.Navigator>
                {state.app.isAuthenticated && state.app.token !== null ? (
                    <>
                        {/*<Stack.Screen options={MainScreen.navigationOptions} name="MainScreen" component={MainScreen}/>*/}
                        <Stack.Screen name="ProfileScreen" component={ProfileScreen}/>
                        <Stack.Screen name="AddressListScreen" component={AddressListScreen}/>
                        <Stack.Screen name="SubscribersList" component={AgreementListScreen}/>
                        <Stack.Screen name="AgreementScreen" component={AgreementScreen}/>
                        <Stack.Screen name="TaskScreen" component={TaskScreen}/>
                        <Stack.Screen name="SubscriberScreen" component={SubscriberScreen}/>
                        <Stack.Screen name="ExecutedTaskListScreen" component={ExecutedTaskListScreen}/>
                        <Stack.Screen name="MainsObjectScreen" component={MainsObjectScreen}/>
                    </>
                ) : (
                    <Stack.Screen options={LoginScreen.navigationOptions} name="Login" component={LoginScreen}/>
                )}
            </Stack.Navigator>
            {loading && <LoaderComponent/>}
        </>
    );


    //=========== NEW NAVIGATION WITH FOOTER TABS =======================
    // const MainStack = createNativeStackNavigator();
    //
    // function MainStackScreen() {
    //     return (
    //         <MainStack.Navigator screenOptions={{headerHideShadow: true}}>
    //             {/*<MainStack.Screen name="MainScreen" component={MainScreen}/>*/}
    //             <MainStack.Screen name="AddressListScreen" component={AddressListScreen}/>
    //             <MainStack.Screen name="SubscribersList" component={AgreementListScreen}/>
    //             <MainStack.Screen name="AgreementScreen" component={AgreementScreen}/>
    //             <MainStack.Screen name="TaskScreen" component={TaskScreen}/>
    //             <MainStack.Screen name="SubscriberScreen" component={SubscriberScreen}/>
    //             <MainStack.Screen name="ExecutedTaskListScreen" component={ExecutedTaskListScreen}/>
    //         </MainStack.Navigator>
    //     );
    // }
    //
    // const MainsObjectStack = createNativeStackNavigator();
    //
    // function MainsObjectStackScreen() {
    //     return (
    //         <MainsObjectStack.Navigator screenOptions={{headerHideShadow: true}}>
    //             <MainsObjectStack.Screen name="MainsObjectScreen" component={MainsObjectScreen}/>
    //         </MainsObjectStack.Navigator>
    //     );
    // }
    //
    // const ProfileStack = createNativeStackNavigator();
    //
    // function ProfileStackScreen() {
    //     return (
    //         <ProfileStack.Navigator screenOptions={{headerHideShadow: true}}>
    //             <ProfileStack.Screen name="ProfileScreen" component={ProfileScreen}/>
    //         </ProfileStack.Navigator>
    //     );
    // }
    //
    // const Tab = createBottomTabNavigator()
    // const LoginStackScreen = createNativeStackNavigator()
    //
    // const tabBarOptions = {
    //     style: {
    //         paddingBottom: 10,
    //         paddingTop: 10,
    //         height: 65
    //     },
    //     labelStyle: {
    //         fontSize: 14
    //     },
    //     inactiveTintColor: NEW_THEME.SECONDARY_COLOR,
    //     activeTintColor: NEW_THEME.PRIMARY_COLOR
    // }
    //
    // return (
    //     <>
    //         {state.app.isAuthenticated && state.app.token !== null ? (
    //             <Tab.Navigator tabBarOptions={tabBarOptions}>
    //                 <Tab.Screen
    //                     name="ProfileScreen"
    //                     component={ProfileStackScreen}
    //                     options={{
    //                         tabBarLabel: 'Профиль',
    //                         tabBarIcon: ({focused}) => (
    //                             <MaterialIcons name="person-outline" size={28} color={focused ? NEW_THEME.PRIMARY_COLOR : NEW_THEME.SECONDARY_COLOR} />
    //                         ),
    //                     }}
    //                 />
    //                 <Tab.Screen
    //                     name="MainsObjectScreen"
    //                     component={MainsObjectStackScreen}
    //                     options={{
    //                         tabBarLabel: 'Фотофиксация',
    //                         tabBarIcon: ({focused}) => (
    //                             <MaterialCommunityIcons name="camera-outline" size={24} color={focused ? NEW_THEME.PRIMARY_COLOR : NEW_THEME.SECONDARY_COLOR}/>
    //                         ),
    //                     }}
    //                 />
    //                 <Tab.Screen
    //                     name="TasksScreen"
    //                     component={MainStackScreen}
    //                     options={{
    //                         tabBarLabel: 'Задачи',
    //                         tabBarIcon: ({focused}) => (
    //                             <MaterialCommunityIcons name="clipboard-text-outline" size={24} color={focused ? NEW_THEME.PRIMARY_COLOR : NEW_THEME.SECONDARY_COLOR} />
    //                         ),
    //                     }}
    //                 />
    //             </Tab.Navigator>
    //         ) : (
    //             <LoginStackScreen.Navigator>
    //                 <LoginStackScreen.Screen name="Login" component={LoginScreen}/>
    //             </LoginStackScreen.Navigator>
    //         )}
    //     </>
    // )
}

export default function AppNavigation() {
    return (
        <NavigationContainer>
            <AppStack/>
        </NavigationContainer>
    );
}