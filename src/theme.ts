export const THEME = {
    PRIMARY_COLOR: '#1F72AD',
    SECONDARY_COLOR: '#619AC1',
    PRIMARY_WITH_OPACITY: 'rgba(31, 114, 173, 0.7)',
    SECONDARY_WITH_OPACITY: 'rgba(97, 154, 193, 0.7)',
    OUTLINE_COLOR: '#C8E0F0',
    PLACEHOLDER_COLOR: '#898989',
    TEXT_COLOR: '#383838',
    SECONDARY_TEXT_COLOR: '#FFFFFF',
    WARNING_COLOR: '#F0AC27',
    ERROR_COLOR: '#E02626',
    INACTIVE_COLOR: '#AECCE1',
    BACKGROUND_COLOR: '#FFFFFF',
    BACKGROUND_WITH_OPACITY: 'rgba(255,255,255,0.7)',
    LOADER_BACKGROUND_COLOR: 'rgba(44,44,44,0.5)',
    HOVER_COLOR: '#194F75',
    SECOND_HOVER_COLOR: 'rgba(25,79,117,0.1)',
    MODAL_BACKGROUND: '#f5f5f5',
    BLACK_BACKGROUND: '#000000',
    INACTIVE_BACKGROUND: '#E4E7EE',
    INACTIVE_TEXT: '#B3B7C2',
}