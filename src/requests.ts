import Store from "./store/store";
import axios from "axios";
import {AppAction} from "./store/app/actions";
import {TaskData, TaskUploadInterface} from "./interfaces";
import {getTaskUploadFormData} from "./helpers";
import {loginApiRequest, tasksApiRequest, tasksProgressApiRequest, uploadTasksApiRequest} from "./api/api";

function clearAuthStore() {
    Store.dispatch(AppAction.clearAuthState());
}

export async function loginRequest(username?: string, password?: string) {
    let request = loginApiRequest(username, password).then(
        result => {
            return {
                type: 'success',
                data: {
                    token: result.data.token,
                    user: result.data.data.user
                }
            }
        }
    ).catch(
        error => {
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTasksListRequest() {
    let authToken = Store.getState().app.token;
    let request = tasksApiRequest(authToken).then(
        tasks => {
            return {
                type: 'success',
                data: tasks.data.tasks,
            };
        }
    ).catch(
        error => {
            console.log('Error: ', error);
            if (error.response.data.code == 401) {
                console.log('Error401: ', error.response.data);
                clearAuthStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function uploadCompletedTaskRequest(id: string, taskData: TaskUploadInterface) {
    let authToken = Store.getState().app.token;
    let formData = getTaskUploadFormData(taskData);

    let request = uploadTasksApiRequest(
        authToken,
        id,
        formData
    ).then(function (data) {
            return {
                type: 'success',
                // data: data.data,
            };
        }
    ).catch(
        error => {
            console.log('Error: ', error);
            if (error.response.data.code == 401) {
                console.log('Error401: ', error.response.data);
                clearAuthStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTasksProgressRequest(id: string) {
    let authToken = Store.getState().app.token;
    let request = tasksProgressApiRequest(authToken, id).then(
        progress => {
            return {
                type: 'success',
                data: progress,
            };
        }
    ).catch(
        error => {
            console.log('Error: ', error);
            if (error.response.data.code == 401) {
                console.log('Error401: ', error.response.data);
                clearAuthStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}