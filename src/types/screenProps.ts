import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

type RootStackParamList = {
    Home: undefined;
    Profile: { userId: string };
    Feed: { sort: 'latest' | 'top' } | undefined;
};

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Profile'>;

type ProfileScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Profile'
    >;

export type ScreenProps = {
    route: ProfileScreenRouteProp;
    navigation: ProfileScreenNavigationProp;
};
//
// export type ScreenProps = {
//     navigation: StackNavigationProp<any, any>;
// };