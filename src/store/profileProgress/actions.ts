import {CHANGE_PROFILE_PROGRESS_DATA} from "./types";
import {profileProgressStateInterface} from "./state";

export interface changeProfileProgressInterface {
    type: typeof CHANGE_PROFILE_PROGRESS_DATA
    payload: profileProgressStateInterface
}

const changeProfileProgressData = (profileProgress: profileProgressStateInterface) => ({
    type: CHANGE_PROFILE_PROGRESS_DATA,
    payload: profileProgress
});

export const ProfileProgressAction = {
    changeProfileProgressData,
};
