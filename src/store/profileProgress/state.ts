export interface profileProgressStateInterface {
    executedTaskCount: number
    taskToWorkCount: number
    taskRatio: number
}

export const initialProfileProgressState: profileProgressStateInterface = {
    executedTaskCount: 0,
    taskToWorkCount: 0,
    taskRatio: 0,
}