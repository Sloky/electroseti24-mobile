import {initialProfileProgressState, profileProgressStateInterface} from "./state";
import {CHANGE_PROFILE_PROGRESS_DATA} from "./types";

const initialState = initialProfileProgressState;

export default function profileProgressReducer(state: profileProgressStateInterface = initialState, action: any) {
    switch (action.type) {
        case CHANGE_PROFILE_PROGRESS_DATA: return {
            ...state,
            ...action.payload,
        }
        default: return state;
    }
}