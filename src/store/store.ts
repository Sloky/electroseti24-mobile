import {AnyAction, applyMiddleware, combineReducers, createStore, Reducer} from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import appReducer from "./app/reducers";
import tasksReducer from "./task/reducers";
import {APP_INIT} from "./app/types";
import {composeWithDevTools} from "redux-devtools-extension";
import {globalStateInterface, saveState} from "./combinedState";
import mainsObjectReducer from "./mainsObect/reducers";
import profileProgressReducer from "./profileProgress/reducers";

const rootReducer:Reducer<globalStateInterface, AnyAction> = combineReducers<globalStateInterface>({
    app: appReducer,
    tasks: tasksReducer,
    mainsObject: mainsObjectReducer,
    profileProgress: profileProgressReducer,
});

// const rootReducer = combineReducers({
//     app: appReducer,
//     tasks: tasksReducer
// });

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = composeWithDevTools({});

const Store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(
        sagaMiddleware
    )));

sagaMiddleware.run(rootSaga);
Store.dispatch({ type: APP_INIT })

Store.subscribe(async () => {
    await saveState(Store.getState()).catch(e => {console.log('Save state error ::', e)});
});

export default Store;