import {appStateInterface} from "./app/state";
import {taskStateInterface} from "./task/state";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {mainsObjectStateInterface} from "./mainsObect/state";
import {profileProgressStateInterface} from "./profileProgress/state";

export interface globalStateInterface {
    app: appStateInterface
    tasks: taskStateInterface
    mainsObject: mainsObjectStateInterface
    profileProgress: profileProgressStateInterface
}

export const getDefaultInitialState = (): globalStateInterface => {
    return {
        app: {
            isAuthenticated: false,
            token: null,
            user: undefined,
            loading: false,
        },
        tasks: {},
        mainsObject: {
            mainsObjects: {}
        },
        profileProgress: {
            executedTaskCount: 0,
            taskToWorkCount: 0,
            taskRatio: 0,
        },
    };
}

export const saveState = async (state: any) => {
    try {
        // console.log('Save to AsyncStorage');
        const serialisedState = JSON.stringify(state);
        await AsyncStorage.setItem('@app_state', serialisedState)
    } catch (error) {
        console.log(error);
    }
};

export const loadState = async () => {
    try {
        // console.log('Load from AsyncStorage');
        const jsonValue = await AsyncStorage.getItem('@app_state')
        return jsonValue ? JSON.parse(jsonValue) : undefined;
    } catch(error) {
        console.log(error);
        return undefined;
    }
};