import {TasksCollection} from "../../interfaces";

// export interface taskStateInterface {
//     usersTasks: usersTasksCollection
// }

export interface userTasksInterface {
    tasks?: TasksCollection
    completedTasks?: TasksCollection
}

export interface taskStateInterface {
    [userId: string]: userTasksInterface
}

export const initialTasksState: taskStateInterface = {};