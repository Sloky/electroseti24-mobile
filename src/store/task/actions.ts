import {
    CHANGE_COMPLETED_TASK,
    CHANGE_COMPLETED_TASK_DATA,
    CHANGE_COMPLETED_TASKS_DATA,
    CHANGE_TASK_DATA,
    CHANGE_TASKS_DATA, CLEAR_TASK_STATE, DELETE_COMPLETED_TASK, DELETE_TASK,
    GET_TASKS_DATA, MOVE_TASK_DATA, REMOVE_TASK_PHOTOS, SET_ALL_USERS_TASKS, UPDATE_TASKS_DATA
} from "./types";
import {TaskData, TasksCollection} from "../../interfaces";
import {taskStateInterface} from "./state";

export interface moveTaskDataActionInterface {
    type: typeof MOVE_TASK_DATA
    taskId: string
    completedTask: TaskData
    userId: string
}

export interface deleteTaskActionInterface {
    type: typeof DELETE_TASK
    taskId: string
    userId: string
}

export interface deleteCompletedTaskActionInterface {
    type: typeof DELETE_COMPLETED_TASK
    taskId: string
    userId: string
}

export interface removeTaskPhotosActionInterface {
    type: typeof REMOVE_TASK_PHOTOS
    photos: string[]
    userId: string
}

export interface changeCompletedTaskDataActionInterface {
    type: typeof CHANGE_COMPLETED_TASK_DATA
    payload: TaskData
    userId: string
}

export interface changeCompletedTaskActionInterface {
    type: typeof CHANGE_COMPLETED_TASK
    payload: TaskData,
    userId: string
}

export interface setAllUsersTasksInterface {
    type: typeof SET_ALL_USERS_TASKS
    payload: taskStateInterface
}

const setAllUsersTasksAction = (allUsersTasks: taskStateInterface): setAllUsersTasksInterface => ({
    type: SET_ALL_USERS_TASKS,
    payload: allUsersTasks
});

const changeTasksAction = (tasksData: TasksCollection|undefined, userId: string) => ({
    type: CHANGE_TASKS_DATA,
    payload: tasksData,
    userId
});

const changeCompletedTasksAction = (completedTasksData: TasksCollection|undefined, userId: string) => (
    {type: CHANGE_COMPLETED_TASKS_DATA, payload: completedTasksData, userId}
);

const updateTasksDataAction = (tasksData: TasksCollection, userId: string) => ({
    type: UPDATE_TASKS_DATA,
    payload: tasksData,
    userId
});

const getTasksDataAction = () => ({type: GET_TASKS_DATA});

const changeTaskDataAction = (task: TaskData, userId: string) => ({
    type: CHANGE_TASK_DATA,
    payload: task,
    userId
});

const changeCompletedTaskDataAction = (task: TaskData, userId: string): changeCompletedTaskDataActionInterface => ({
    type: CHANGE_COMPLETED_TASK_DATA,
    payload: task,
    userId
});

const changeCompletedTaskAction = (task: TaskData, userId: string): changeCompletedTaskActionInterface => ({
    type: CHANGE_COMPLETED_TASK,
    payload: task,
    userId
});

const moveTaskDataAction = (taskId: string, completedTask: TaskData, userId: string): moveTaskDataActionInterface => ({
    type: MOVE_TASK_DATA,
    taskId,
    completedTask,
    userId
});
const deleteTaskAction = (taskId: string, userId: string): deleteTaskActionInterface => ({
    type: DELETE_TASK,
    taskId,
    userId
});

const deleteCompletedTaskAction = (taskId: string, userId: string): deleteCompletedTaskActionInterface => ({
    type: DELETE_COMPLETED_TASK,
    taskId,
    userId
});

const removeTaskPhotosAction = (photos: string[], userId: string): removeTaskPhotosActionInterface => ({
    type: REMOVE_TASK_PHOTOS,
    photos,
    userId
});

const clearTaskState = () => ({type: CLEAR_TASK_STATE});

export const TasksAction = {
    setAllUsersTasksAction,

    changeTasksAction,
    changeCompletedTasksAction,

    getTasksDataAction,

    changeTaskDataAction,
    changeCompletedTaskDataAction,
    changeCompletedTaskAction,

    updateTasksDataAction,

    moveTaskDataAction,
    deleteTaskAction,
    deleteCompletedTaskAction,

    removeTaskPhotosAction,

    clearTaskState
}