import {all, fork, put, takeEvery} from "redux-saga/effects";
import {
    CHANGE_COMPLETED_TASK_DATA,
    MOVE_TASK_DATA,
    REMOVE_ALL_FILES,
    REMOVE_TASK_PHOTOS,
    SHOW_ALL_FILES
} from "./types";
import {
    changeCompletedTaskDataActionInterface,
    moveTaskDataActionInterface,
    removeTaskPhotosActionInterface,
    TasksAction
} from "./actions";
import {removeAllTheFiles, removePhotos} from "../../helpers";
import * as FileSystem from "expo-file-system";


function* moveTaskWorker(action: moveTaskDataActionInterface) {
    yield put(TasksAction.changeCompletedTaskDataAction(action.completedTask, action.userId));
    yield put(TasksAction.deleteTaskAction(action.taskId, action.userId));
}

function* removeAllFilesWorker() {
    removeAllTheFiles().then(r => {
        console.log('All the files have been removed');
    });
}

function* removePhotosWorker(action: removeTaskPhotosActionInterface) {
    removePhotos(action.photos).then(r => {
        console.log('Photos have been removed');
    });
}

function* showAllFilesWorker() {
    const docDirUri = FileSystem.documentDirectory || '/';
    FileSystem.readDirectoryAsync(docDirUri).then((files: string[]) => {
        console.log('Folder files', files);
    });
}

function* changeCompletedTaskWorker(action: changeCompletedTaskDataActionInterface) {
    const date = new Date();
    let completedTask = {...action.payload};
    completedTask.execution_date_time = date.toISOString();
    yield put(TasksAction.changeCompletedTaskAction(completedTask, action.userId));
}

function* watchMoveTask() {
    yield takeEvery(MOVE_TASK_DATA, moveTaskWorker)
}

function* watchRemoveAllFiles() {
    yield takeEvery(REMOVE_ALL_FILES, removeAllFilesWorker)
}

function* watchRemovePhotos() {
    yield takeEvery(REMOVE_TASK_PHOTOS, removePhotosWorker)
}

function* watchShowAllFiles() {
    yield takeEvery(SHOW_ALL_FILES, showAllFilesWorker)
}

function* watchChangeCompletedTask() {
    yield takeEvery(CHANGE_COMPLETED_TASK_DATA, changeCompletedTaskWorker)
}

export function* taskSaga() {
    yield all([
        fork(watchMoveTask),
        fork(watchRemoveAllFiles),
        fork(watchRemovePhotos),
        fork(watchShowAllFiles),
        fork(watchChangeCompletedTask),
    ]);
}