import {initialTasksState, taskStateInterface} from "./state";
import {
    CHANGE_COMPLETED_TASK,
    CHANGE_COMPLETED_TASK_DATA,
    CHANGE_COMPLETED_TASKS_DATA, CHANGE_TASK_DATA,
    CHANGE_TASKS_DATA, CLEAR_TASK_STATE, DELETE_COMPLETED_TASK, DELETE_TASK, SET_ALL_USERS_TASKS, UPDATE_TASKS_DATA
} from "./types";
import {deleteTaskFromState} from "../../helpers";
import Store from "../store";

const initialState = initialTasksState;

export default function tasksReducer(state: taskStateInterface = initialState, action: any) {
    // let userId = Store?.getState().app.user?.id;

    // if (userId !== undefined) {
        switch (action.type) {
            case CLEAR_TASK_STATE: return {};
            case SET_ALL_USERS_TASKS: return {
                ...state,
                ...action.payload
            };
            case CHANGE_TASKS_DATA: return {
                ...state,
                [action.userId]: {...state[action.userId], tasks: action.payload}
            };
            case CHANGE_COMPLETED_TASKS_DATA: return {
                ...state,
                [action.userId]: {...state[action.userId], completedTasks: action.payload}
            };
            case CHANGE_TASK_DATA: return {
                ...state,
                [action.userId]: {...state[action.userId], tasks: {...state[action.userId]?.tasks || {}, [action.payload.id]: action.payload}}
            };
            case CHANGE_COMPLETED_TASK: return {
                ...state,
                [action.userId]: {...state[action.userId], completedTasks: {...state[action.userId]?.completedTasks || {}, [action.payload.id]: action.payload}}
            };
            case UPDATE_TASKS_DATA: return  {
                ...state,
                [action.userId]: {...state[action.userId], tasks: {...state[action.userId]?.tasks || {}, ...action.payload}}
            };
            case DELETE_TASK: return {
                ...state,
                [action.userId]: {...state[action.userId], tasks: deleteTaskFromState(state[action.userId]?.tasks, action.taskId)}
            };
            case DELETE_COMPLETED_TASK: return {
                ...state,
                [action.userId]: {...state[action.userId], completedTasks: deleteTaskFromState(state[action.userId]?.completedTasks, action.taskId)}
            };
            default : return state;
        }
    // } else {
    //     return state;
    // }
}