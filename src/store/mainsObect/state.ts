import {CoordinateInterface} from "../../interfaces";

export interface mainsObjectStateInterface {
    mainsObjects: MainsObjectCollection
}

export interface MainsObjectInterface {
    photoName: string
    uri: string
    dateTime: string
    gps: CoordinateInterface
    userName: string
}

export interface MainsObjectCollection {
    [id: string]: MainsObjectInterface
}

export const initialMainsObjectState: mainsObjectStateInterface = {
    mainsObjects: {}
};
