import {
    CHANGE_MAINS_OBJECT_DATA,
    CHANGE_MAINS_OBJECTS_DATA,
    DELETE_MAINS_OBJECT,
    DELETE_MAINS_OBJECTS, REMOVE_MAINS_OBJECT_FILE,
    REMOVE_MAINS_OBJECT_FILES
} from "./types";
import {MainsObjectCollection, MainsObjectInterface} from "./state";

export interface removeMainsObjectFilesActionInterface {
    type: typeof REMOVE_MAINS_OBJECT_FILES
}

export interface removeMainsObjectFileActionInterface {
    type: typeof REMOVE_MAINS_OBJECT_FILE
    fileUri: string
}

export interface changeMainsObject {
    type: typeof CHANGE_MAINS_OBJECT_DATA
    payload: MainsObjectInterface
}

export interface changeMainsObjectsInterface {
    type: typeof CHANGE_MAINS_OBJECTS_DATA
    payload: MainsObjectCollection
}

export interface deleteMainsObjectInterface {
    type: typeof DELETE_MAINS_OBJECT
    photoName: string
}

export interface deleteMainsObjectsInterface {
    type: typeof DELETE_MAINS_OBJECTS
}

const changeMainsObjectData = (mainsObject: MainsObjectInterface): changeMainsObject => ({
    type: CHANGE_MAINS_OBJECT_DATA,
    payload: mainsObject
});

const changeMainsObjectsData = (mainsObjects: MainsObjectCollection): changeMainsObjectsInterface => ({
    type: CHANGE_MAINS_OBJECTS_DATA,
    payload: mainsObjects
});

const deleteMainsObjectData = (photoName: string): deleteMainsObjectInterface => ({
    type: DELETE_MAINS_OBJECT,
    photoName
});

const deleteMainsObjectsData = (): deleteMainsObjectsInterface => ({
    type: DELETE_MAINS_OBJECTS
});

const removeMainsObjectFilesAction = (): removeMainsObjectFilesActionInterface => ({type: REMOVE_MAINS_OBJECT_FILES});
const removeMainsObjectFileAction = (fileUri: string): removeMainsObjectFileActionInterface => ({
    type: REMOVE_MAINS_OBJECT_FILE,
    fileUri
});

export const MainsObjectAction = {
    removeMainsObjectFilesAction,
    removeMainsObjectFileAction,

    changeMainsObjectsData,
    changeMainsObjectData,

    deleteMainsObjectData,
    deleteMainsObjectsData
}