import {initialMainsObjectState} from "./state";
import {CHANGE_MAINS_OBJECT_DATA, CHANGE_MAINS_OBJECTS_DATA, DELETE_MAINS_OBJECT, DELETE_MAINS_OBJECTS} from "./types";
import {deleteMainsObjectFromState} from "../../helpers";


const initialState = initialMainsObjectState;

export default function mainsObjectReducer(state = initialState, action: any): any {
    switch (action.type) {
        case CHANGE_MAINS_OBJECTS_DATA: return {
            ...state,
            mainsObjects: action.payload,
        }
        case CHANGE_MAINS_OBJECT_DATA: return {
            ...state,
            mainsObjects: {...state.mainsObjects, [action.payload.photoName]: action.payload},
        }
        case DELETE_MAINS_OBJECT: return {
            ...state,
            mainsObjects: deleteMainsObjectFromState(state.mainsObjects, action.photoName)
        }
        case DELETE_MAINS_OBJECTS: return {
            ...state,
            mainsObjects: {}
        }
        default : return state;
    }
}