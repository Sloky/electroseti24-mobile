import {all, fork, put, takeEvery} from "redux-saga/effects";
import {REMOVE_MAINS_OBJECT_FILE, REMOVE_MAINS_OBJECT_FILES} from "./types";
import {removeMainsObjectFile, removeMainsObjectFiles} from "../../helpers";
import {removeMainsObjectFileActionInterface} from "./actions";

function* removeMainsObjectFilesWorker() {
    removeMainsObjectFiles().then(r => {
        console.log('All the mains object files have been removed');
    });
}

function* removeMainsObjectFileWorker(action: removeMainsObjectFileActionInterface) {
    removeMainsObjectFile(action.fileUri).then(r => {
        console.log('The mains object file has been removed');
    });
}

function* watchRemoveMainsObjectFiles() {
    yield takeEvery(REMOVE_MAINS_OBJECT_FILES, removeMainsObjectFilesWorker)
}

function* watchRemoveMainsObjectFile() {
    yield takeEvery(REMOVE_MAINS_OBJECT_FILE, removeMainsObjectFileWorker)
}

export function* mainsObjectSaga() {
    yield all([
        fork(watchRemoveMainsObjectFiles),
        fork(watchRemoveMainsObjectFile),
    ]);
}