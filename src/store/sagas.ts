import {all, fork} from 'redux-saga/effects';
import {appSaga} from "./app/sagas";
import {taskSaga} from "./task/sagas";
import {mainsObjectSaga} from "./mainsObect/sagas";

export default function* rootSaga() {
    yield all([
        fork(appSaga),
        fork(taskSaga),
        fork(mainsObjectSaga)
    ]);
}