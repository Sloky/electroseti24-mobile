import {
    APP_CLEAR_AUTH_STATE,
    APP_CLEAR_STATE,
    AUTH_ERROR,
    AUTH_LOGIN,
    AUTH_LOGOUT,
    AUTH_SUCCESS,
    CHANGE_AUTH_STATUS,
    CHANGE_TOKEN,
    CHANGE_USER_DATA, HIDE_LOADER, SHOW_LOADER
} from "./types";
import {UserInterface} from "./state";

export interface changeTokenActionInterface {
    type: typeof CHANGE_TOKEN,
    payload: string|null
}

export interface changeAuthStatusActionInterface {
    type: typeof CHANGE_AUTH_STATUS,
    payload: boolean
}

export interface changeUserDataActionInterface {
    type: typeof CHANGE_USER_DATA,
    payload: UserInterface|undefined
}

export interface loginActionInterface {
    type: typeof AUTH_LOGIN,
    username: string
    password: string
}

export interface loginSuccessActionInterface {
    type: typeof AUTH_SUCCESS,
    token: string
    status: boolean
    userData: UserInterface
}

export interface loginErrorActionInterface {
    type: typeof AUTH_ERROR
    error: any
}

export interface logoutActionInterface {
    type: typeof AUTH_LOGOUT
}

export interface showLoaderInterface {
    type: typeof SHOW_LOADER
}

export interface hideLoaderInterface {
    type: typeof HIDE_LOADER
}

const changeTokenAction = (token: string|null): changeTokenActionInterface => ({type: CHANGE_TOKEN, payload: token});
const changeAuthStatusAction = (status: boolean): changeAuthStatusActionInterface => ({type: CHANGE_AUTH_STATUS, payload: status});
const changeUserDataAction = (userData: UserInterface|undefined): changeUserDataActionInterface => ({type: CHANGE_USER_DATA, payload: userData});

const clearState = () => ({type: APP_CLEAR_STATE});
const clearAuthState = () => ({type: APP_CLEAR_AUTH_STATE});

const loginAction = (username: string, password: string): loginActionInterface => ({type: AUTH_LOGIN, username, password});
const loginSuccessAction = (token: string, status: boolean, userData: UserInterface): loginSuccessActionInterface => ({
    type: AUTH_SUCCESS,
    token,
    status,
    userData
});
const loginErrorAction = (error: any): loginErrorActionInterface => ({type: AUTH_ERROR, error});
const logoutAction = (): logoutActionInterface => ({type: AUTH_LOGOUT});

const showLoader = (): showLoaderInterface => ({type: SHOW_LOADER});
const hideLoader = (): hideLoaderInterface => ({type: HIDE_LOADER});

export const AppAction = {
    changeTokenAction,
    changeAuthStatusAction,
    changeUserDataAction,

    clearState,
    clearAuthState,

    loginAction,
    loginSuccessAction,
    loginErrorAction,
    logoutAction,

    showLoader,
    hideLoader,
}