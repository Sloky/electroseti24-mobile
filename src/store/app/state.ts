export interface appStateInterface {
    isAuthenticated: boolean,
    token: string|null,
    user?: UserInterface,
    loading?: boolean,
}

export interface UserInterface {
    id: string
    username: string
    name: string
    surname: string
    patronymic: string
    companies: string[]
    work_status: number
    address: string
    roles: string[]
    groups: object[]
    phone: string
}

export const initialAppState: appStateInterface = {
    isAuthenticated: false,
    token: null,
    user: <UserInterface>{},
    loading: false,
};
