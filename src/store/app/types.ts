const randomString = () =>
    Math.random().toString(36).substring(7).split('').join('.')

export const APP_INIT = `@@redux/INIT${/* #__PURE__ */ randomString()}`;
export const APP_CLEAR_STATE = 'APP_CLEAR_STATE';

export const APP_CLEAR_AUTH_STATE = 'APP_CLEAR_AUTH_STATE';

export const CHANGE_TOKEN = 'CHANGE_TOKEN';
export const CHANGE_AUTH_STATUS = 'CHANGE_AUTH_STATUS';
export const CHANGE_USER_DATA = 'CHANGE_USER_DATA';

export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
