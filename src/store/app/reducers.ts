import {appStateInterface, initialAppState} from "./state";
import {
    APP_CLEAR_AUTH_STATE,
    APP_CLEAR_STATE,
    APP_INIT,
    CHANGE_AUTH_STATUS,
    CHANGE_TOKEN,
    CHANGE_USER_DATA,
    HIDE_LOADER,
    SHOW_LOADER
} from "./types";
import {getDefaultInitialState, globalStateInterface} from "../combinedState";

const initialState = initialAppState;

export default function appReducer(state = initialState, action: any): any {
    // console.debug('## appReducer', action.type, action, state)
    switch (action.type) {
        case APP_INIT: return {
                ...state
        };
        case APP_CLEAR_AUTH_STATE: return {
            ...state,
            token: '',
            isAuthenticated: false,
            user: undefined
        }
        case CHANGE_TOKEN: return {
            ...state,
            token: action.payload,
        };
        case CHANGE_AUTH_STATUS: return {
            ...state,
            isAuthenticated: action.payload,
        };
        case CHANGE_USER_DATA: return {
            ...state,
            user: action.payload,
        };
        case SHOW_LOADER: return {
            ...state,
            loading: true,
        };
        case HIDE_LOADER: return {
            ...state,
            loading: false,
        };
        default : return state;
    }
}