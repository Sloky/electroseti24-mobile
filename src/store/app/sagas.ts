import {APP_CLEAR_STATE, APP_INIT, AUTH_ERROR, AUTH_SUCCESS} from "./types";
import {all, call, put, fork, takeEvery, select} from "redux-saga/effects"
import {AppAction, loginErrorActionInterface, loginSuccessActionInterface} from "./actions";
import {TasksAction} from "../task/actions";
import {getDefaultInitialState, globalStateInterface, loadState} from "../combinedState";
import {MainsObjectAction} from "../mainsObect/actions";
import {tasksApiRequest} from "../../api/api";
import {TaskData} from "../../interfaces";
import {getTasksCollection, getUnchangeableTasks} from "../../helpers";

export function* getInitState(action: any) {
    const data: globalStateInterface = yield call(async (action: any) => {
        let stateData = await loadState();
        if (stateData) {
            return stateData;
        }
            return getDefaultInitialState();
    }, action);
    yield put(AppAction.changeTokenAction(data.app.token));
    yield put(AppAction.changeAuthStatusAction(data.app.isAuthenticated));
    yield put(AppAction.changeUserDataAction(data.app.user));
    yield put(TasksAction.setAllUsersTasksAction(data.tasks));
    // yield put(TasksAction.changeCompletedTasksAction(data.tasks.completedTasks));
    yield put(MainsObjectAction.changeMainsObjectsData(data.mainsObject.mainsObjects));
}

function* clearAllState() {
    yield put(AppAction.clearAuthState())
    yield put(TasksAction.clearTaskState())
}

function* loginSuccessWorker(action: loginSuccessActionInterface) {
    yield put(AppAction.changeTokenAction(action.token));
    yield put(AppAction.changeAuthStatusAction(action.status));
    yield put(AppAction.changeUserDataAction(action.userData));

    const tasks = yield select((state: globalStateInterface) => state.tasks[action.userData.id]?.tasks);
    const completedTasks = yield select((state: globalStateInterface) => state.tasks[action.userData.id]?.completedTasks);

    const result = yield tasksApiRequest(action.token).then(
        tasks => {
            return {
                type: 'success',
                data: tasks.data.tasks,
            };
        }
    ).catch(
        error => {
            console.log('Error: ', error);
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );

    if (result.type == 'success') {
        console.log('request success');
        let unchangeableTasksIds = getUnchangeableTasks(Object.values({...tasks, ...completedTasks}))
        let refreshedTasks = result.data.filter((task: TaskData) => {
            return !unchangeableTasksIds.find((unchangeableTask: string) => unchangeableTask == task.id);
        });
        yield put(TasksAction.updateTasksDataAction(getTasksCollection(refreshedTasks), action.userData.id));
    } else {
        console.log('Task request error: ', result.data);
    }
}

function* loginErrorWorker(action: loginErrorActionInterface) {
    console.log('loginErrorWorker', action.error);
}

function* watchInitApp() {
    yield takeEvery(APP_INIT, getInitState)
}

function* watchClearAllState() {
    yield takeEvery(APP_CLEAR_STATE, clearAllState)
}

function* watchLoginSuccess() {
    yield takeEvery(AUTH_SUCCESS, loginSuccessWorker)
}

function* watchLoginError() {
    yield takeEvery(AUTH_ERROR, loginErrorWorker)
}

export function* appSaga() {
    yield all([
        fork(watchInitApp),
        fork(watchClearAllState),
        fork(watchLoginSuccess),
        fork(watchLoginError),
    ]);
}