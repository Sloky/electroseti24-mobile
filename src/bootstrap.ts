import * as Font from 'expo-font'

export async function bootstrap() {
    await Font.loadAsync({
        'roboto-medium': require('../assets/fonts/Roboto-Medium.ttf'),
        'roboto-regular': require('../assets/fonts/Roboto-Regular.ttf')
    })
}