import React, {useEffect, useLayoutEffect, useState} from "react";
import {ActivityIndicator, Dimensions, Keyboard, Text, View} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import {TopRightMenu} from "../../components/TopRightMenu/TopRightMenu";
import {styles} from "./ExecutedTaskListScreenStyles";
import SubscriberTypeModal from "../AddressListScreen/SubscriberTypeModal";
import {TaskData, TasksCollection} from "../../interfaces";
import {Button} from "@ui-kitten/components";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import {ExecutedTaskList} from "./ExecutedTaskList/ExecutedTaskList";
import {ProgressBar} from "../../components/ProgressBar/ProgressBar";
import {getTasksProgressRequest, uploadCompletedTaskRequest} from "../../requests";
import {TasksAction} from "../../store/task/actions";
import {getTaskUploadData} from "../../helpers";
import {TaskMenu} from "../../components/TaskMenu/TaskMenu";
import Footer from "../../components/Footer/Footer";
import SearchComponent from "../../components/SearchComponent/SearchComponent";
import {ProfileProgressAction} from "../../store/profileProgress/actions";

interface filterInterface {
    subscriberTypes: number[]
    search?: string
}

function applyTaskFilter(tasksData: TaskData[], filter: filterInterface) {
    let tasks = tasksData.filter(
        task => filter.subscriberTypes.find(value => task.subscriber_type == value) !== undefined
    );
    return tasks;
}

function applyTaskSearches(tasksData: TaskData[], searchValue: string) {
    let reqExp = new RegExp(searchValue, 'i');
    let tasks = tasksData.filter(
        task =>
            reqExp.test(task.subscriber_title) ||
            reqExp.test(task.counter_number) ||
            reqExp.test(task.agreement_number) ||
            reqExp.test(task.locality) ||
            reqExp.test(task.street) ||
            reqExp.test(task.house_number)
    );
    return tasks;
}

const screenProps = {
    button1: {
        title: 'Задачи к выполнению',
        link: 'AddressListScreen',
    },
    button2: {
        title: 'Выполненные задачи',
        link: 'ExecutedTaskListScreen',
        active: true
    },
}

export function ExecutedTaskListScreen({navigation, route}: any) {
    const [loader, setLoader] = useState<boolean>(false);
    const [filter, setFilter] = useState<filterInterface>({subscriberTypes: [0, 1, 2]});
    const [filteredTasks, setFilteredTasks] = useState<TaskData[]>();
    const [showProgBar, setShowProgBar] = useState<boolean>(false);
    const [progress, setProgress] = useState<{ percent: number, text: string }>({
        percent: 0,
        text: '0%'
    });
    const [backdrop, setBackdrop] = useState<boolean>(false);
    const [keyboardStatus, setKeyboardStatus] = useState<boolean | undefined>(undefined);

    const dispatch = useDispatch();
    const window = Dimensions.get("window");

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let completedTasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.completedTasks);

    const onSubscriberTypeChange = (types: number[]) => {
        setFilter({subscriberTypes: types});
    };

    const onChangeSearchValue = (searchValue: string) => {
        if (completedTasks) {
            setFilteredTasks(applyTaskSearches(Object.values(completedTasks), searchValue));
        }
    };

    const onSubmitCompletedTasks = () => {
        if (filteredTasks && filteredTasks.length > 0) {
            let count = filteredTasks.length;
            let counter = 0;
            setShowProgBar(true);
            filteredTasks.map(task => {
                let uploadData = getTaskUploadData(task);
                uploadCompletedTaskRequest(task.id, uploadData).then(
                    response => {
                        if (response.type == 'success') {
                            dispatch(TasksAction.removeTaskPhotosAction(task.photos, userId));
                            dispatch(TasksAction.deleteCompletedTaskAction(task.id, userId));
                        }
                    }
                ).finally(() => {
                    let percent = Math.floor((++counter / count) * 100);
                    setProgress({
                        percent: percent,
                        text: percent.toString() + '%'
                    });
                    if (percent >= 100) {
                        setShowProgBar(false);
                    }
                });
            });

        }
    };

    useEffect(() => {
        getTasksProgressRequest(userId).then(result => {
            if (result.type === 'success') {
                dispatch(ProfileProgressAction.changeProfileProgressData(result.data.data));
            } else {
                dispatch(ProfileProgressAction.changeProfileProgressData({
                    executedTaskCount: 0,
                    taskRatio: 0,
                    taskToWorkCount: 0,
                }));
            }
        })
    }, [filteredTasks])

    useEffect(() => {
        if (completedTasks) {
            setFilteredTasks(applyTaskFilter(Object.values(completedTasks), filter));
        }
    }, [completedTasks, filter]);

    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Выполненые задачи',
            headerStyle: {
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: backdrop ? THEME.TEXT_COLOR : THEME.BACKGROUND_COLOR,
            headerLeft: () => (<View/>),
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation, backdrop]);

    return (
        <>
            {!keyboardStatus && <TaskMenu button1={screenProps.button1} button2={screenProps.button2}/>}
            <View style={styles.wrapper}>
                <View style={styles.content}>

                    <SubscriberTypeModal
                        onSubscriberTypeChange={onSubscriberTypeChange}
                        initialTypes={filter.subscriberTypes}
                        setBackdrop={setBackdrop}
                    />
                    <View style={styles.listHeader}>
                        <Text style={styles.listHeaderText}>Узлы учёта</Text>
                    </View>

                    {showProgBar &&
                    <ProgressBar
                        progress={progress.percent}
                        text={progress.text}
                    />
                    }

                    {!loader ? (
                        <ExecutedTaskList
                            listData={filteredTasks || []}
                        />
                    ) : (
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR}/>
                        </View>
                    )}

                </View>
                <SearchComponent onChangeSearchValue={onChangeSearchValue} searchPosition={keyboardStatus ? 3 : 14}/>
                {!keyboardStatus &&
                <View style={styles.buttonBlock}>
                    <Button
                        status={'primary'}
                        size={'medium'}
                        onPress={onSubmitCompletedTasks}
                        activeOpacity={0.7}
                    >
                        Выгрузить данные обхода
                    </Button>
                </View>}
            </View>
            {!keyboardStatus && <Footer activeBtn={'Tasks'}/>}
            <View style={{
                position: 'absolute',
                height: backdrop ? window.height : 0,
                width: '100%',
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : 'transparent'
            }}/>
        </>
    );
}
