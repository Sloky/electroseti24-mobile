import {StyleSheet} from "react-native";
import {THEME} from "../../../theme";

export const styles = StyleSheet.create({
    flatList: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20,
    },
    item: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: THEME.PRIMARY_COLOR,
        borderRadius: 10,
        marginBottom: 20,
        paddingVertical: 10,
        width: '100%',
    },
    itemText: {
        color: THEME.PRIMARY_COLOR,
        fontSize: 18,
    },
});