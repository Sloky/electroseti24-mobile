import React, {useState} from "react";
import {TaskData} from "../../../interfaces";
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./ExecutedTaskListStyles";
import {useNavigation} from "@react-navigation/core";

interface ExecutedTaskListProps {
    listData: TaskData[]
}

export function ExecutedTaskList(props: ExecutedTaskListProps) {
    const [refreshing, setRefreshing] = useState(false);

    const navigation = useNavigation();

    const onListRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {setRefreshing(false)}, 1000);
    };

    const onCounterButtonPress = (taskId: string) => {
        navigation.navigate('TaskScreen', {task_id: taskId, isCompletedTask: true});
    };

    return (
        <>
            <FlatList
                style={styles.flatList}
                contentContainerStyle={{alignItems: 'center'}}
                data={props.listData.reverse()}
                keyExtractor={item => item.counter_number+'_'+item.period}
                refreshing={refreshing}
                onRefresh={onListRefresh}
                ListEmptyComponent={
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={{textAlign: 'center'}}>Нет данных</Text>
                    </View>
                }
                renderItem={value => {
                    return (
                        <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity
                                style={styles.item}
                                key={value.item.counter_number+'_'+value.item.period}
                                onPress={() => onCounterButtonPress(value.item.id)}
                            >
                                <Text style={styles.itemText}>{value.item.counter_number}</Text>
                            </TouchableOpacity>
                        </View>
                    );
                }}
            />
        </>
    );
}