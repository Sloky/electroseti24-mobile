import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    content: {
        flex: 1,
    },
    buttonBlock: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    listHeader: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        paddingHorizontal: 20,
        marginBottom: 15,
    },
    listHeaderText: {
        display: 'flex',
        fontSize: 15,
        fontWeight: '700',
        textAlign: 'left',
        textAlignVertical: 'center'
    },
});