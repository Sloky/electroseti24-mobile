import {StyleSheet} from "react-native";
import {THEME} from "../../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    counterValue: {
        marginBottom: 10,
    },
    formInput: {
        marginBottom: 10,
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderColor: THEME.OUTLINE_COLOR,
    },
    button: {
        margin: 10
    },
    photoBtn: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderColor: THEME.WARNING_COLOR,
    },
    photoBtnDisabled: {
        color: THEME.INACTIVE_TEXT,
        backgroundColor: THEME.INACTIVE_BACKGROUND,
        borderColor: THEME.INACTIVE_TEXT,
    },
});