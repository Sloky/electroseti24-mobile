import React, {MutableRefObject} from "react";
import {View} from "react-native";
import {styles} from "./CounterValueBoxStyles";
import {Button, Input, useTheme} from "@ui-kitten/components";
import {PhotoPicker} from "../../../components/PhotoPicker/PhotoPicker";
import {Formik} from "formik";
import * as Yup from "yup";
import {THEME} from "../../../theme";

export interface CounterValueBoxValues {
    counter_value: string
    counter_value_photo: string
    controller_comment: string
}

interface CounterValueBoxProps {
    taskId: string
    initialValues: CounterValueBoxValues
    onSubmit(values: CounterValueBoxValues): void
    saveAlert?: MutableRefObject<boolean>
}

export function CounterValueBox(props: CounterValueBoxProps) {
    const validationSchema = Yup.object().shape({
        counter_value: Yup.string().matches(/^\d+([\.\,]\d+)?$/),
        counter_value_photo: Yup.string().required(),
        controller_comment: Yup.string().notRequired()
    });

    const theme = useTheme();
    return (
        <View style={styles.wrapper}>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}

                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <View style={styles.counterValue}>
                            <Input
                                style={styles.formInput}
                                label={'Показания счетчика'}
                                placeholder={'Внесите показания'}
                                keyboardType={"decimal-pad"}
                                onChangeText={(value) => {
                                    if (value.trim()) {
                                        props.saveAlert ? props.saveAlert.current = true : null;
                                    } else {
                                        props.saveAlert ? props.saveAlert.current = false : null;
                                    }
                                    setFieldValue('counter_value', value);
                                }}
                                status={errors.counter_value ? 'danger' : 'basic'}
                                value={values.counter_value}
                            />
                            {/*{(dirty && !errors.counter_value) && (*/}
                            <PhotoPicker
                                onMakePhoto={(photo) => {
                                setFieldValue('counter_value_photo', photo);
                            }}
                                isNeedLocation={true}
                                buttonActiveOpacity={0.7}
                                buttonAppearance={'outline'}
                                buttonIconColor={THEME.WARNING_COLOR}
                                buttonStyleStatus={'warning'}
                                styleBtn={!dirty ? styles.photoBtnDisabled : styles.photoBtn}
                                isClickable={true}
                                isDisabled={!dirty}
                            />
                            {/*)}*/}
                            <Input
                                style={styles.formInput}
                                label={'Комментарий контролёра'}
                                multiline={true}
                                onChangeText={(value) => {
                                    if (value.trim()) {
                                        props.saveAlert ? props.saveAlert.current = true : null;
                                    } else {
                                        props.saveAlert ? props.saveAlert.current = false : null;
                                    }
                                    setFieldValue('controller_comment', value);
                                }}
                                status={errors.controller_comment ? 'danger' : 'basic'}
                                value={values.controller_comment}
                                placeholder={'Внесите комментарий'}
                            />
                        </View>
                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            activeOpacity={0.7}
                            size={'medium'}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </View>
    );
}