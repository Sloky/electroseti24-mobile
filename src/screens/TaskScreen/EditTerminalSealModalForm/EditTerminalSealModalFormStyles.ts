import {StyleSheet} from "react-native";
import {THEME} from "../../../theme";

export const styles = StyleSheet.create({
    button: {
        marginVertical: 10,
        backgroundColor: THEME.BACKGROUND_COLOR,
    }
});