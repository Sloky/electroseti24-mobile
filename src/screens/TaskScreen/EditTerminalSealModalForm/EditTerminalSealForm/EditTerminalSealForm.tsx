import React from "react";
import {View, TouchableOpacity, Text, Switch} from "react-native";
import * as Yup from "yup";
import {Formik} from "formik";
import {Button, Input, Spinner, Toggle} from "@ui-kitten/components";
import {PhotoPicker} from "../../../../components/PhotoPicker/PhotoPicker";
import {styles} from "./EditTerminalSealFormStyles";
import {THEME} from "../../../../theme";

export interface EditTerminalSealFormValues {
    terminal_seal: string
    counter_physical_status: boolean
    terminal_seal_photo: string
}

interface EditCounterFormProps {
    initialValues: EditTerminalSealFormValues

    onSubmit(values: EditTerminalSealFormValues): void
}

export function EditTerminalSealForm(props: EditCounterFormProps) {
    const validationSchema = Yup.object().shape({
        terminal_seal: Yup.string().nullable(true),
        counter_physical_status: Yup.boolean().required(),
        terminal_seal_photo: Yup.string().required(),
    });

    return (
        <>
            <Text style={styles.modalTitle}>Клеммная пломба</Text>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}

                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <TouchableOpacity style={styles.modalToggle} onPress={() => {
                            setFieldValue('counter_physical_status', !values.counter_physical_status)
                        }}>
                            <Text style={styles.modalText}>{values.counter_physical_status ? 'Пломба исправна' : 'Пломба неисправна'}</Text>
                            <Toggle
                                checked={values.counter_physical_status}
                                // onChange={(value) => {
                                //     setFieldValue('counter_physical_status', value);
                                // }}
                                onChange={() => {
                                    setFieldValue('counter_physical_status', !values.counter_physical_status)
                                }}
                            />
                        </TouchableOpacity>
                        <Input
                            style={styles.formInput}
                            label={'Номер клеммной пломбы'}
                            onChangeText={handleChange('terminal_seal')}
                            status={errors.terminal_seal ? 'danger' : 'basic'}
                            value={values.terminal_seal}
                        />
                        {/*{dirty && (*/}
                        <PhotoPicker
                            onMakePhoto={(photo) => {
                            setFieldValue('terminal_seal_photo', photo);
                        }}
                            isNeedLocation={true}
                            buttonActiveOpacity={0.7}
                            buttonAppearance={'outline'}
                            buttonIconColor={THEME.WARNING_COLOR}
                            buttonStyleStatus={'warning'}
                            styleBtn={!dirty ? styles.photoBtnDisabled : styles.photoBtn}
                            isDisabled={!dirty}
                        />
                        {/*)}*/}
                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            activeOpacity={0.7}
                            size={'medium'}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </>
    );
}