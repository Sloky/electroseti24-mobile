import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import {TaskData} from "../../../interfaces";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../../store/combinedState";
import {Button} from "@ui-kitten/components";
import SimpleModal from "../../../components/SimpleModal/SimpleModal";
import {ActivityIndicator, View} from "react-native";
import {THEME} from "../../../theme";
import {EditTerminalSealForm, EditTerminalSealFormValues} from "./EditTerminalSealForm/EditTerminalSealForm";
import * as FileSystem from "expo-file-system";
import {getCurrentPosition, getObjCompDifField} from "../../../helpers";
import {TasksAction} from "../../../store/task/actions";
import {styles} from "./EditTerminalSealModalFormStyles";
import {useNavigation} from "@react-navigation/core";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";
import {Feather} from "@expo/vector-icons";
import {AppAction} from "../../../store/app/actions";

interface EditTerminalSealModalFormProps {
    taskId: string
    isCompletedTask: boolean
    taskCount: number
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

export function EditTerminalSealModalForm(props: EditTerminalSealModalFormProps) {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [task, setTask] = useState<TaskData>();

    const navigation = useNavigation();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return props.isCompletedTask ? state.tasks[userId].completedTasks : state.tasks[userId].tasks;
    });

    const dispatch = useDispatch();

    const onOpenModal = () => {
        props.setBackdrop(true);
        setShowModal(true);
    };

    const onCloseModal = () => {
        props.setBackdrop(false);
        setShowModal(false);
    };

    const onTerminalSealFormSubmit = async (values: EditTerminalSealFormValues) => {
        props.setBackdrop(false);
        setShowModal(false);
        dispatch(AppAction.showLoader());

        const fileName = values.terminal_seal_photo.split('/').pop();

        let location = await getCurrentPosition();

        //Если пломба неисправна, меняем имя фотографии
        const newPath = values.counter_physical_status
            ? FileSystem.documentDirectory + 'terminal_seal_photo_' + fileName
            : FileSystem.documentDirectory + 'counter_photo_' + fileName;

        try {
            await FileSystem.moveAsync({
                from: values.terminal_seal_photo,
                to: newPath
            });
        } catch (e) {
            console.log(e);
        }

        if (task) {
            let changedFields = getObjCompDifField(
                {
                    terminal_seal: task.terminal_seal,
                    counter_physical_status: task.counter_physical_status,
                },
                {
                    terminal_seal: values.terminal_seal,
                    counter_physical_status: (values.counter_physical_status ? 0 : 1),
                }
            );

            let changedTask = {...task};
            changedTask.terminal_seal = values.terminal_seal;
            changedTask.counter_physical_status = (values.counter_physical_status ? 0 : 1);
            changedTask.photos.push(newPath);
            changedTask.changed_fields.push(...changedFields);
            changedTask.newCoordinates = {
                GPSLatitude: location.coords.latitude,
                GPSLongitude: location.coords.longitude
            }

            if (props.isCompletedTask) {
                dispatch(TasksAction.changeCompletedTaskDataAction(changedTask, userId));
                dispatch(AppAction.hideLoader());
            } else {
                if (changedTask.counter_physical_status == 1) {
                    changedTask.status = 1;
                    dispatch(TasksAction.moveTaskDataAction(task.id, changedTask, userId))
                    dispatch(AppAction.hideLoader());
                    if (props.taskCount > 1) {
                        navigation.goBack();
                    } else {
                        navigation.navigate('AddressListScreen');
                    }
                } else {
                    dispatch(TasksAction.changeTaskDataAction(changedTask, userId));
                    dispatch(AppAction.hideLoader());
                }
            }
        } else {
            dispatch(AppAction.hideLoader());
        }
    };

    useEffect(() => {
        if (tasks) {
            setTask(tasks[props.taskId]);
        }
    }, [tasks]);

    const editIcon = () => (
        <Feather name="edit" size={24} color={THEME.PRIMARY_COLOR} />
    )

    return (
        <>
            <Button
                appearance={'outline'}
                style={styles.button}
                size={'medium'}
                status={''}
                accessoryLeft={editIcon}
                onPress={onOpenModal}
                activeOpacity={0.7}
            >
                Изменить
            </Button>
            <SimpleModal
                isVisible={showModal}
                onCloseModal={onCloseModal}
            >
                {task ? (
                    <EditTerminalSealForm
                        initialValues={{
                            counter_physical_status: (task.counter_physical_status == 0),
                            terminal_seal: task.terminal_seal,
                            terminal_seal_photo: ''
                        }}
                        onSubmit={onTerminalSealFormSubmit}
                    />
                ) : (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}
            </SimpleModal>
        </>
    );
}