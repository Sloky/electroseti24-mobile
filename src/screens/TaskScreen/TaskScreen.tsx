import React, {useEffect, useLayoutEffect, useRef, useState} from "react";
import {ActivityIndicator, Alert, ScrollView, Text, TouchableOpacity, View, Keyboard, Dimensions} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import {useNavigation, useRoute} from "@react-navigation/core";
import {styles} from "./TaskScreenStyles";
import {IDictionary, TaskData} from "../../interfaces";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import EditableBox from "../../components/EditableBox/EditableBox";
import {EditCounterModalForm} from "./EditCounterModalForm/EditCounterModalForm";
import {EditAntiMagneticSealModalForm} from "./EditAntiMagneticSealModalForm/EditAntiMagneticSealModalForm";
import {EditSideSealModalForm} from "./EditSideSealModalForm/EditSideSealModalForm";
import {EditTerminalSealModalForm} from "./EditTerminalSealModalForm/EditTerminalSealModalForm";
import {CounterValueBox, CounterValueBoxValues} from "./CounterValueBox/CounterValueBox";
import * as FileSystem from "expo-file-system";
import {changeFloatingPoint, getCurrentPosition, getFullAddress, getImagesByPrefix} from "../../helpers";
import {TasksAction} from "../../store/task/actions";
import {EditCounterValueModalForm} from "./EditCounterValueModalForm/EditCounterValueModalForm";
// import {TopRightMenu} from "../../components/TopRightMenu/TopRightMenu";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";
import Footer from "../../components/Footer/Footer";
import {AntDesign, Entypo} from '@expo/vector-icons';
import {AppAction} from "../../store/app/actions";

const stringCounterPhysicalStatuses: IDictionary<string> = {
    '0': 'Исправен/Доступен',
    '1': 'Неисправен/Недоступен'
}

export function TaskScreen() {
    const [task, setTask] = useState<TaskData>();
    const [tabVisible, setTabVisible] = useState<boolean>(false);
    const [keyboardStatus, setKeyboardStatus] = useState<boolean | undefined>(undefined)
    const [backdrop, setBackdrop] = useState<boolean>(false);

    const saveAlert = useRef<boolean>(false);
    const route: any = useRoute();
    const task_id = route.params.task_id;
    const isCompletedTask: boolean = route.params.isCompletedTask || false;
    const taskCount = route.params.taskCount;
    const window = Dimensions.get("window");

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return isCompletedTask ? state.tasks[userId]?.completedTasks: state.tasks[userId]?.tasks;
    });

    const dispatch = useDispatch();

    const navigation = useNavigation();

    const onCounterValueSubmit = async (values: CounterValueBoxValues) => {
        dispatch(AppAction.showLoader());
        const fileName = values.counter_value_photo.split('/').pop();
        const newPath = FileSystem.documentDirectory + 'counter_value_photo_' + fileName;

        let location = await getCurrentPosition();

        try {
            await FileSystem.moveAsync({
                from: values.counter_value_photo,
                to: newPath
            });
        } catch (e) {
            Alert.alert('Произошла ошибка при сохранении фотографии. Повторите попытку');
            console.log(e);
        }

        let counterValue = changeFloatingPoint(values.counter_value);

        if (task) {
            let changedTask = {...task};
            /*changedTask.current_counter_value
                ? changedTask.current_counter_value = [Number(values.counter_value)]
                : changedTask.current_counter_value = [Number(values.counter_value)];*/
            values.controller_comment && (changedTask.controller_comment = values.controller_comment);
            changedTask.current_counter_value = [Number(counterValue)];
            changedTask.photos.push(newPath);
            changedTask.status = 1;
            changedTask.newCoordinates = {
                GPSLatitude: location.coords.latitude,
                GPSLongitude: location.coords.longitude
            }
            dispatch(TasksAction.moveTaskDataAction(task.id, changedTask, userId));
            dispatch(AppAction.hideLoader());
            if (taskCount > 1) {
                navigation.goBack();
            } else {
                navigation.navigate('AddressListScreen');
            }
        } else {
            dispatch(AppAction.hideLoader());
        }
    };

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: isCompletedTask ? 'Карточка задачи' : 'Снятие показаний',
            headerStyle: {
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
            headerBackTitle: ' ',
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation, backdrop]);

    useEffect(() => {
        if (tasks) {
            setTask(tasks[task_id]);
        }
    }, [tasks]);

    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    return (
        <View style={styles.screenWrapper}>
            <View style={styles.contentWrapper}>
                {task ? (
                    <>
                        <View style={styles.contentHeader}>
                            <Text style={styles.contentHeaderCounter}>{`Узел учёта ${task.counter_number}`}</Text>
                            <Text style={styles.contentHeaderSubscriber}>{task.subscriber_title}</Text>
                            <Text style={styles.contentHeaderAgreement}>{task.agreement_number}</Text>
                            <Text style={styles.contentHeaderAddress}>{getFullAddress(task)}</Text>
                            {task.operator_comment &&
                                <TouchableOpacity style={styles.commentTab} onPress={() => setTabVisible(!tabVisible)}>
                                    <View style={styles.commentTitleWrapper}>
                                        <View style={styles.commentTitleContainer}>
                                            <AntDesign name="exclamationcircle" size={24} color={THEME.WARNING_COLOR} />
                                            <Text style={styles.commentTitle}>Сообщение от оператора</Text>
                                        </View>
                                        {tabVisible
                                            ? <Entypo style={{marginRight: 10}} name="chevron-up" size={24} color={THEME.WARNING_COLOR}/>
                                            : <Entypo style={{marginRight: 10}} name="chevron-down" size={24} color={THEME.WARNING_COLOR}/>
                                        }
                                    </View>
                            {task.operator_comment && tabVisible &&
                                (<View style={styles.commentTextContainer}>
                                    <Text style={styles.commentText}>{task.operator_comment}</Text>
                                </View>)
                            }
                                </TouchableOpacity>
                            }

                        </View>
                        <ScrollView style={{}} contentContainerStyle={styles.contentBody}>
                            {/*<EditableBox*/}
                            {/*    items={[*/}
                            {/*        {*/}
                            {/*            title: 'Подстанция',*/}
                            {/*            value: task.substation ? task.substation.title : task.temporary_substation || 'Нет данных',*/}
                            {/*        },*/}
                            {/*        {*/}
                            {/*            title: 'Фидер',*/}
                            {/*            value: task.feeder ? task.feeder.title : task.temporary_feeder || 'Нет данных',*/}
                            {/*        },*/}
                            {/*        {*/}
                            {/*            title: 'ТП',*/}
                            {/*            value: task.transformer ? task.transformer.title : task.temporary_transformer || 'Нет данных',*/}
                            {/*        },*/}
                            {/*        {*/}
                            {/*            title: '№ Линии',*/}
                            {/*            value: task.line_number || 'Нет данных',*/}
                            {/*        },*/}
                            {/*        {*/}
                            {/*            title: '№ Столба',*/}
                            {/*            value: task.electric_pole_number || 'Нет данных',*/}
                            {/*        },*/}
                            {/*    ]}*/}
                            {/*/>*/}
                            <Text style={styles.sectionTitle}>Счётчик</Text>
                            <EditableBox
                                modal={
                                    <EditCounterModalForm
                                        taskId={task && task.id}
                                        isCompletedTask={isCompletedTask}
                                        taskCount={taskCount}
                                        setBackdrop={setBackdrop}
                                    />
                                }
                                images={getImagesByPrefix(task.photos, 'counter_photo')}
                                items={[
                                    {
                                        title: 'Состояние',
                                        value: stringCounterPhysicalStatuses[task.counter_physical_status.toString()]
                                    },
                                    {
                                        title: 'Тип счётчика',
                                        value: task.counter_type ? task.counter_type.title: task.temporary_counter_type || 'Нет данных',
                                    },
                                    {
                                        title: 'Номер счётчика',
                                        value: task.counter_number
                                    },
                                ]}
                            />
                            <Text style={styles.sectionTitle}>Пломбы</Text>
                            <EditableBox
                                modal={
                                    <EditTerminalSealModalForm
                                        taskId={task && task.id}
                                        isCompletedTask={isCompletedTask}
                                        taskCount={taskCount}
                                        setBackdrop={setBackdrop}
                                    />
                                }
                                images={getImagesByPrefix(task.photos, 'terminal_seal_photo')}
                                items={[
                                    {
                                        title: 'Клеммная пломба',
                                        value: task.terminal_seal || 'Нет данных',
                                    },
                                ]}
                            />
                            <EditableBox
                                modal={
                                    <EditAntiMagneticSealModalForm
                                        taskId={task && task.id}
                                        isCompletedTask={isCompletedTask}
                                        taskCount={taskCount}
                                        setBackdrop={setBackdrop}
                                    />
                                }
                                images={getImagesByPrefix(task.photos, 'anti_magnetic_seal_photo')}
                                items={[
                                    {
                                        title: 'Антимагнитная пломба',
                                        value: task.anti_magnetic_seal || 'Нет данных',
                                    },
                                ]}
                            />
                            <EditableBox
                                modal={
                                    <EditSideSealModalForm
                                        taskId={task && task.id}
                                        isCompletedTask={isCompletedTask}
                                        taskCount={taskCount}
                                        setBackdrop={setBackdrop}
                                    />
                                }
                                images={getImagesByPrefix(task.photos, 'side_seal_photo')}
                                items={[
                                    {
                                        title: 'Боковая пломба',
                                        value: task.side_seal || 'Нет данных',
                                    },
                                ]}
                            />
                            <Text style={styles.sectionTitle}>Снятие показаний счётчика</Text>
                            {(task.status == 0) && (task.counter_physical_status == 0)? (
                                <CounterValueBox
                                    taskId={task && task.id}
                                    initialValues={{
                                        counter_value: '',
                                        counter_value_photo: '',
                                        controller_comment: ''
                                    }}
                                    onSubmit={onCounterValueSubmit}
                                    saveAlert={saveAlert}
                                />
                            ) : (
                                <EditableBox
                                    modal={
                                        <EditCounterValueModalForm
                                            taskId={task && task.id}
                                            isCompletedTask={isCompletedTask}
                                            setBackdrop={setBackdrop}
                                        />
                                    }
                                    images={getImagesByPrefix(task.photos, 'counter_value_photo')}
                                    items={[
                                        {
                                            title: 'Показания счётчика',
                                            value: task.current_counter_value ? task.current_counter_value.toString() : 'Отсутствуют'
                                        },
                                        {
                                            title: 'Примечание',
                                            value: task.controller_comment || 'Отсутствует'
                                        }
                                    ]}
                                />
                            )}

                        </ScrollView>
                    </>
                ) : (
                    <View style={styles.spinner}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}

            </View>
            {!keyboardStatus && <Footer activeBtn={'Tasks'} saveAlert={saveAlert}/>}
            <View style={{
                position: 'absolute',
                height: backdrop ? window.height : 0,
                width: '100%',
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : 'transparent'
            }}/>
        </View>

    );
}