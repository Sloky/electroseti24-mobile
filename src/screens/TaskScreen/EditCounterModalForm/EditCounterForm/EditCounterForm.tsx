import React from "react";
import {Text, TouchableOpacity} from "react-native";
import {styles} from "./EditCounterFormStyles";
import {Formik} from 'formik';
import * as Yup from "yup";
import {Button, Input, Toggle} from "@ui-kitten/components";
import {PhotoPicker} from "../../../../components/PhotoPicker/PhotoPicker";
import {THEME} from "../../../../theme";

export interface EditCounterFormValues {
    counter_number: string
    counter_type_title: string
    counter_physical_status: boolean
    counter_photo: string
}

interface EditCounterFormProps {
    initialValues: EditCounterFormValues

    onSubmit(values: EditCounterFormValues): void
}

export function EditCounterForm(props: EditCounterFormProps) {
    const validationSchema = Yup.object().shape({
        counter_number: Yup.string().required(),
        counter_type_title: Yup.string().required(),
        counter_physical_status: Yup.boolean().required(),
        counter_photo: Yup.string().required(),
    });

    return (
        <>
            <Text style={styles.modalTitle}>Счётчик</Text>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <TouchableOpacity style={styles.modalToggle} onPress={() => {
                            setFieldValue('counter_physical_status', !values.counter_physical_status)
                        }}>
                            <Text style={styles.modalText}>{values.counter_physical_status ? 'Счётчик исправен' : 'Счётчик неисправен'}</Text>
                            <Toggle
                                checked={values.counter_physical_status}
                                onChange={() => {
                                    setFieldValue('counter_physical_status', !values.counter_physical_status)
                                }}
                            />
                        </TouchableOpacity>
                        <Input
                            style={styles.formInput}
                            label={'Номер счётчика'}
                            onChangeText={handleChange('counter_number')}
                            status={errors.counter_number ? 'danger' : 'basic'}
                            value={values.counter_number}
                        />
                        <Input
                            style={styles.formInput}
                            label={'Тип счётчика'}
                            onChangeText={handleChange('counter_type_title')}
                            status={errors.counter_type_title ? 'danger' : 'basic'}
                            value={values.counter_type_title}
                        />
                        {/*{dirty && (*/}
                            <PhotoPicker
                                onMakePhoto={(photo) => {
                                    setFieldValue('counter_photo', photo);
                                }}
                                isNeedLocation={true}
                                buttonActiveOpacity={0.7}
                                buttonAppearance={'outline'}
                                buttonIconColor={THEME.WARNING_COLOR}
                                buttonStyleStatus={'warning'}
                                styleBtn={!dirty ? styles.photoBtnDisabled : styles.photoBtn}
                                isDisabled={!dirty}
                            />
                        {/*)}*/}
                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            activeOpacity={0.7}
                            size={'medium'}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </>
    );
}