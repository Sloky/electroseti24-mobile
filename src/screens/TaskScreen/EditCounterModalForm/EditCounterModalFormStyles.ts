import {StyleSheet} from "react-native";
import {THEME} from "../../../theme";

export const styles = StyleSheet.create({
    button: {
        marginVertical: 10,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    spinnerBackground: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
});