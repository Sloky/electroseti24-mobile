import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import SimpleModal from "../../../components/SimpleModal/SimpleModal";
import {EditCounterForm, EditCounterFormValues} from "./EditCounterForm/EditCounterForm";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../../store/combinedState";
import {TaskData} from "../../../interfaces";
import {ActivityIndicator, View} from "react-native";
import {THEME} from "../../../theme";
import {Button} from "@ui-kitten/components";
import * as FileSystem from "expo-file-system";
import {getCurrentPosition, getObjCompDifField} from "../../../helpers";
import {TasksAction} from "../../../store/task/actions";
import {styles} from "./EditCounterModalFormStyles";
import {useNavigation} from "@react-navigation/core";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";
import {Feather} from '@expo/vector-icons';
import {AppAction} from "../../../store/app/actions";

interface EditCounterModalFormProps {
    taskId: string
    isCompletedTask: boolean
    taskCount: number
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

export function EditCounterModalForm(props: EditCounterModalFormProps) {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [task, setTask] = useState<TaskData>();

    const navigation = useNavigation();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return props.isCompletedTask ? state.tasks[userId].completedTasks : state.tasks[userId].tasks;
    });

    const dispatch = useDispatch();

    const onOpenModal = () => {
        props.setBackdrop(true);
        setShowModal(true);
    }

    const onCloseModal = () => {
        props.setBackdrop(false);
        setShowModal(false);
    };

    const onCounterFormSubmit = async (values: EditCounterFormValues) => {
        props.setBackdrop(false);
        setShowModal(false);
        dispatch(AppAction.showLoader());
        const fileName = values.counter_photo.split('/').pop();
        const newPath = FileSystem.documentDirectory + 'counter_photo_' + fileName;

        let location = await getCurrentPosition();

        try {
            await FileSystem.moveAsync({
                from: values.counter_photo,
                to: newPath
            });
        } catch (e) {
            console.log(e);
        }

        if (task) {
            let changedFields = getObjCompDifField(
                {
                    counter_number: task.counter_number,
                    counter_physical_status: task.counter_physical_status,
                    counter_type_title: task.counter_type ? task.counter_type.title : task.temporary_counter_type || ''
                },
                {
                    counter_number: values.counter_number,
                    counter_physical_status: (values.counter_physical_status ? 0 : 1),
                    counter_type_title: values.counter_type_title
                }
            );

            let changedTask = {...task};
            changedTask.counter_number = values.counter_number;
            changedTask.counter_physical_status = (values.counter_physical_status ? 0 : 1);
            task.counter_type
                ? changedTask.counter_type.title = values.counter_type_title
                : changedTask.counter_type = {
                    title: values.counter_type_title,
                    capacity: null,
                    class_accuracy: 1,
                    electron_current_type: 1,
                    number_phases: 1,
                    tariff_type: '1',
                    indicator_device: null,
                    number_tariffs: null,
                    operating_mechanism: null
                }
            changedTask.photos.push(newPath);
            changedTask.changed_fields.push(...changedFields);
            changedTask.newCoordinates = {
                GPSLatitude: location.coords.latitude,
                GPSLongitude: location.coords.longitude
            }

            if (props.isCompletedTask) {
                dispatch(TasksAction.changeCompletedTaskDataAction(changedTask, userId));
                dispatch(AppAction.hideLoader());
            } else {
                if (changedTask.counter_physical_status == 1) {
                    changedTask.status = 1;
                    dispatch(TasksAction.moveTaskDataAction(task.id, changedTask, userId))
                    dispatch(AppAction.hideLoader());
                    if (props.taskCount > 1) {
                        navigation.goBack();
                    } else {
                        navigation.navigate('AddressListScreen');
                    }
                } else {
                    dispatch(TasksAction.changeTaskDataAction(changedTask, userId));
                    dispatch(AppAction.hideLoader());
                }
            }
        } else {
            dispatch(AppAction.hideLoader());
        }
    };

    useEffect(() => {
        if (tasks) {
            setTask(tasks[props.taskId]);
        }
    }, [tasks]);

    const editIcon = () => (
        <Feather name="edit" size={24} color={THEME.PRIMARY_COLOR} />
    )

    return (
        <>
            <Button
                appearance={'outline'}
                style={styles.button}
                size={'medium'}
                status={''}
                accessoryLeft={editIcon}
                onPress={onOpenModal}
                activeOpacity={0.7}
            >
                Изменить
            </Button>
            <SimpleModal
                isVisible={showModal}
                onCloseModal={onCloseModal}
            >
                {task ? (
                    <EditCounterForm
                        initialValues={{
                            counter_physical_status: (task.counter_physical_status == 0),
                            counter_type_title: task.counter_type ? task.counter_type.title : task.temporary_counter_type || '',
                            counter_number: task.counter_number,
                            counter_photo: ''
                        }}
                        onSubmit={onCounterFormSubmit}
                    />
                ) : (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}
            </SimpleModal>
        </>

    );
}