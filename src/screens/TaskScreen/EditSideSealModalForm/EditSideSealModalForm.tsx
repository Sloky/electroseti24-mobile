import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import {TaskData} from "../../../interfaces";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../../store/combinedState";
import {Button} from "@ui-kitten/components";
import SimpleModal from "../../../components/SimpleModal/SimpleModal";
import {ActivityIndicator, View} from "react-native";
import {THEME} from "../../../theme";
import {styles} from "./EditSideSealModalFormStyles";
import {EditSideSealForm, EditSideSealFormValues} from "./EditSideSealForm/EditSideSealForm";
import * as FileSystem from "expo-file-system";
import {getCurrentPosition, getObjCompDifField} from "../../../helpers";
import {TasksAction} from "../../../store/task/actions";
import {useNavigation} from "@react-navigation/core";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";
import {Feather} from "@expo/vector-icons";
import {AppAction} from "../../../store/app/actions";

interface EditSideSealModalFormProps {
    taskId: string
    isCompletedTask: boolean
    taskCount: number
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

export function EditSideSealModalForm(props: EditSideSealModalFormProps) {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [task, setTask] = useState<TaskData>();

    const navigation = useNavigation();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return props.isCompletedTask ? state.tasks[userId].completedTasks : state.tasks[userId].tasks;
    });

    const dispatch = useDispatch();

    const onOpenModal = () => {
        props.setBackdrop(true);
        setShowModal(true);
    };

    const onCloseModal = () => {
        props.setBackdrop(false);
        setShowModal(false);
    };

    const onSideSealFormSubmit = async (values: EditSideSealFormValues) => {
        props.setBackdrop(false);
        setShowModal(false);
        dispatch(AppAction.showLoader());
        const fileName = values.side_seal_photo.split('/').pop();

        let location = await getCurrentPosition();

        //Если пломба неисправна, меняем имя фотографии
        const newPath = values.counter_physical_status
            ? FileSystem.documentDirectory + 'side_seal_photo_' + fileName
            : FileSystem.documentDirectory + 'counter_photo_' + fileName;

        try {
            await FileSystem.moveAsync({
                from: values.side_seal_photo,
                to: newPath
            });
        } catch (e) {
            console.log(e);
        }

        if (task) {
            let changedFields = getObjCompDifField(
                {
                    side_seal: task.side_seal,
                    counter_physical_status: task.counter_physical_status,
                },
                {
                    side_seal: values.side_seal,
                    counter_physical_status: (values.counter_physical_status ? 0 : 1),
                }
            );

            let changedTask = {...task};
            changedTask.side_seal = values.side_seal;
            changedTask.counter_physical_status = (values.counter_physical_status ? 0 : 1);
            changedTask.photos.push(newPath);
            changedTask.changed_fields.push(...changedFields);
            changedTask.newCoordinates = {
                GPSLatitude: location.coords.latitude,
                GPSLongitude: location.coords.longitude
            }

            if (props.isCompletedTask) {
                dispatch(TasksAction.changeCompletedTaskDataAction(changedTask, userId));
                dispatch(AppAction.hideLoader());
            } else {
                if (changedTask.counter_physical_status == 1) {
                    changedTask.status = 1;
                    dispatch(TasksAction.moveTaskDataAction(task.id, changedTask, userId))
                    dispatch(AppAction.hideLoader());
                    if (props.taskCount > 1) {
                        navigation.goBack();
                    } else {
                        navigation.navigate('AddressListScreen');
                    }
                } else {
                    dispatch(TasksAction.changeTaskDataAction(changedTask, userId));
                    dispatch(AppAction.hideLoader());
                }
            }
        } else {
            dispatch(AppAction.hideLoader());
        }
    };

    useEffect(() => {
        if (tasks) {
            setTask(tasks[props.taskId]);
        }
    }, [tasks]);

    const editIcon = () => (
        <Feather name="edit" size={24} color={THEME.PRIMARY_COLOR} />
    )

    return (
        <>
            <Button
                appearance={'outline'}
                style={styles.button}
                size={'medium'}
                status={''}
                accessoryLeft={editIcon}
                onPress={onOpenModal}
                activeOpacity={0.7}
            >
                Изменить
            </Button>
            <SimpleModal
                isVisible={showModal}
                onCloseModal={onCloseModal}
            >
                {task ? (
                    <EditSideSealForm
                        initialValues={{
                            counter_physical_status: (task.counter_physical_status == 0),
                            side_seal: task.side_seal,
                            side_seal_photo: ''
                        }}
                        onSubmit={onSideSealFormSubmit}
                    />
                ) : (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}
            </SimpleModal>
        </>
    );
}