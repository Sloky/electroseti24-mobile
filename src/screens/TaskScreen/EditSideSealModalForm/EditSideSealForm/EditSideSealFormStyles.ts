import {StyleSheet} from "react-native";
import {THEME} from "../../../../theme";

export const styles = StyleSheet.create({
    modalTitle: {
        alignSelf: 'center',
        marginBottom: 10,
        fontWeight: '700',
        fontSize: 16,
        color: THEME.TEXT_COLOR,
    },
    formInput: {
        marginBottom: 10,
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderColor: THEME.OUTLINE_COLOR,
    },
    spinnerBackground: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalToggle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: THEME.OUTLINE_COLOR,
        borderRadius: 6,
        padding: 5,
        marginBottom: 10,
    },
    modalText: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
    photoBtn: {
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    photoBtnDisabled: {
        color: THEME.INACTIVE_TEXT,
        backgroundColor: THEME.INACTIVE_BACKGROUND,
        borderColor: THEME.INACTIVE_TEXT,
    },
});