import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import * as Yup from "yup";
import {Formik} from "formik";
import {Button, Input, Spinner, Toggle} from "@ui-kitten/components";
import {PhotoPicker} from "../../../../components/PhotoPicker/PhotoPicker";
import {styles} from "./EditAntiMagneticSealFormStyles";
import {THEME} from "../../../../theme";

export interface EditAntiMagneticSealFormValues {
    anti_magnetic_seal: string
    counter_physical_status: boolean
    anti_magnetic_seal_photo: string
}

interface EditAntiMagneticSealFormProps {
    initialValues: EditAntiMagneticSealFormValues

    onSubmit(values: EditAntiMagneticSealFormValues): void
}

export function EditAntiMagneticSealForm(props: EditAntiMagneticSealFormProps) {
    const validationSchema = Yup.object().shape({
        anti_magnetic_seal: Yup.string().nullable(),
        counter_physical_status: Yup.boolean().required(),
        anti_magnetic_seal_photo: Yup.string().required(),
    });

    return (
        <>
            <Text style={styles.modalTitle}>Антимагнитная пломба</Text>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}

                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <TouchableOpacity style={styles.modalToggle} onPress={() => {
                            setFieldValue('counter_physical_status', !values.counter_physical_status)
                        }}>
                            <Text style={styles.modalText}>{values.counter_physical_status ? 'Пломба исправна' : 'Пломба неисправна'}</Text>
                            <Toggle
                                checked={values.counter_physical_status}
                                // onChange={(value) => {
                                //     setFieldValue('counter_physical_status', value);
                                // }}
                                onChange={() => {
                                    setFieldValue('counter_physical_status', !values.counter_physical_status)
                                }}
                            />
                        </TouchableOpacity>
                        <Input
                            style={styles.formInput}
                            label={'Номер антимагнитной пломбы'}
                            onChangeText={handleChange('anti_magnetic_seal')}
                            status={errors.anti_magnetic_seal ? 'danger' : 'basic'}
                            value={values.anti_magnetic_seal}
                        />
                        {/*{dirty && (*/}
                        <PhotoPicker
                            onMakePhoto={(photo) => {
                            setFieldValue('anti_magnetic_seal_photo', photo);
                        }}
                            isNeedLocation={true}
                            buttonActiveOpacity={0.7}
                            buttonAppearance={'outline'}
                            buttonIconColor={THEME.WARNING_COLOR}
                            buttonStyleStatus={'warning'}
                            styleBtn={!dirty ? styles.photoBtnDisabled : styles.photoBtn}
                            isDisabled={!dirty}
                        />
                        {/*)}*/}
                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            activeOpacity={0.7}
                            size={'medium'}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </>
    );
}