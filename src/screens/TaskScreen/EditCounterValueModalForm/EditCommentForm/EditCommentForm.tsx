import React from "react";
import * as Yup from "yup";
import {Formik} from "formik";
import {Button, Input} from "@ui-kitten/components";
import {Text, View} from "react-native";
import {styles} from "./EditCommentFormStyles";
import {THEME} from "../../../../theme";

export interface EditCommentFormValues {
    controller_comment: string
}

interface EditCommentFormProps {
    initialValues: EditCommentFormValues

    onSubmit(values: EditCommentFormValues): void
}

export function EditCommentForm(props: EditCommentFormProps) {
    const validationSchema = Yup.object().shape({
        controller_comment: Yup.string(),
    });

    return (
        <Formik
            initialValues={props.initialValues}
            validationSchema={validationSchema}

            onSubmit={(values) => {
                props.onSubmit(values);
            }}
        >
            {({
                  handleChange,
                  handleSubmit,
                  setFieldValue,
                  dirty,
                  isValid,
                  errors,
                  values
              }) => (
                <>
                    <Text
                        style={{color: 'red', marginBottom: 10}}
                    >Счётчик неисправен. Внесение показаний невозможно.</Text>
                    <Input
                        style={styles.formInput}
                        label={'Комментарий контролёра'}
                        multiline={true}
                        onChangeText={handleChange('controller_comment')}
                        status={errors.controller_comment ? 'danger' : 'basic'}
                        value={values.controller_comment}
                    />

                    <Button
                        onPress={() => handleSubmit()}
                        disabled={!(isValid && dirty)}
                        status={'primary'}
                        size={'medium'}
                        activeOpacity={0.7}
                    >
                        Сохранить
                    </Button>
                </>
            )}
        </Formik>
    );
}