import React from "react";
import {Text, View} from "react-native";
import * as Yup from "yup";
import {Formik} from "formik";
import {Button, Input, Toggle} from "@ui-kitten/components";
import {PhotoPicker} from "../../../../components/PhotoPicker/PhotoPicker";
import {styles} from "./EditCounterValueFormStyles";
import {number} from "yup";
import {THEME} from "../../../../theme";

export interface EditCounterValueFormValues {
    counter_value: string
    controller_comment: string
    counter_value_photo: string
}

interface EditCounterFormProps {
    initialValues: EditCounterValueFormValues

    onSubmit(values: EditCounterValueFormValues): void
}

export function EditCounterValueForm(props: EditCounterFormProps) {
    const validationSchema = Yup.object().shape({
        counter_value: Yup.string().matches(/^\d+(\.\d+)?$/),
        controller_comment: Yup.string(),
        counter_value_photo: Yup.string().required(),
    });

    return (
        <>
            <Text style={styles.modalTitle}>Снятие показаний счётчика</Text>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}

                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <Input
                            style={styles.formInput}
                            label={'Показания счётчика'}
                            keyboardType={"decimal-pad"}
                            onChangeText={(value) => {
                                setFieldValue('counter_value', value);
                                setFieldValue('counter_value_photo', '');
                            }}
                            status={errors.counter_value ? 'danger' : 'basic'}
                            value={values.counter_value}
                        />

                        <Input
                            style={styles.formInput}
                            label={'Комментарий контролёра'}
                            multiline={true}
                            onChangeText={handleChange('controller_comment')}
                            status={errors.controller_comment ? 'danger' : 'basic'}
                            value={values.controller_comment}
                        />

                        {/*{dirty && (*/}
                        <PhotoPicker
                            onMakePhoto={(photo) => {
                            setFieldValue('counter_value_photo', photo);
                        }}
                            isNeedLocation={true}
                            buttonActiveOpacity={0.7}
                            buttonAppearance={'outline'}
                            buttonIconColor={THEME.WARNING_COLOR}
                            buttonStyleStatus={'warning'}
                            styleBtn={!dirty ? styles.photoBtnDisabled : styles.photoBtn}
                            isDisabled={!dirty}
                        />
                        {/*)}*/}

                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            activeOpacity={0.7}
                            size={'medium'}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </>
    );
}