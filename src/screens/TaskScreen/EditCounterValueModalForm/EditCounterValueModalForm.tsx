import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import {ActivityIndicator, Alert, View} from "react-native";
import {TaskData} from "../../../interfaces";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../../store/combinedState";
import {Button} from "@ui-kitten/components";
import {styles} from "../EditCounterModalForm/EditCounterModalFormStyles";
import SimpleModal from "../../../components/SimpleModal/SimpleModal";
import {EditCounterForm} from "../EditCounterModalForm/EditCounterForm/EditCounterForm";
import {THEME} from "../../../theme";
import {EditCounterValueForm, EditCounterValueFormValues} from "./EditCounterValueForm/EditCounterValueForm";
import {EditCommentForm, EditCommentFormValues} from "./EditCommentForm/EditCommentForm";
import * as FileSystem from "expo-file-system";
import {TasksAction} from "../../../store/task/actions";
import {getCurrentPosition, getImagesByPrefix} from "../../../helpers";
import * as Location from "expo-location";
import {LocationAccuracy} from "expo-location";
import {Feather} from "@expo/vector-icons";
import {AppAction} from "../../../store/app/actions";

interface EditCounterValueModalFormProps {
    taskId: string
    isCompletedTask: boolean
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

const getCounterValuePhotoForForm = (task: TaskData): string => {
    let photos = getImagesByPrefix(task.photos, 'counter_value');
    return photos ? photos.reverse()[0] : '';
};

export function EditCounterValueModalForm(props: EditCounterValueModalFormProps) {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [task, setTask] = useState<TaskData>();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return props.isCompletedTask ? state.tasks[userId].completedTasks : state.tasks[userId].tasks;
    });

    const dispatch = useDispatch();

    const onOpenModal = () => {
        props.setBackdrop(true);
        setShowModal(true);
    };

    const onCloseModal = () => {
        props.setBackdrop(false);
        setShowModal(false);
    };

    const onCounterValueFormSubmit = async (values: EditCounterValueFormValues) => {
        props.setBackdrop(false);
        setShowModal(false);
        dispatch(AppAction.showLoader());
        const fileName = values.counter_value_photo.split('/').pop();
        const newPath = FileSystem.documentDirectory + 'counter_value_photo_' + fileName;

        let location = await getCurrentPosition();

        try {
            await FileSystem.moveAsync({
                from: values.counter_value_photo,
                to: newPath
            });
        } catch (e) {
            Alert.alert('Произошла ошибка при сохранении фотографии. Повторите попытку');
            console.log(e);
        }

        if (task) {
            let changedTask = {...task};
            changedTask.current_counter_value
                ? changedTask.current_counter_value = [Number(values.counter_value)]
                : changedTask.current_counter_value = [Number(values.counter_value)];
            changedTask.photos.push(newPath);
            changedTask.controller_comment = values.controller_comment;
            changedTask.newCoordinates = {
                GPSLatitude: location.coords.latitude,
                GPSLongitude: location.coords.longitude
            }

            if (props.isCompletedTask) {
                dispatch(TasksAction.changeCompletedTaskDataAction(changedTask, userId));
                dispatch(AppAction.hideLoader());
            } else {
                dispatch(TasksAction.changeTaskDataAction(changedTask, userId));
                dispatch(AppAction.hideLoader());
            }
        }
    }

    const onCommentFormSubmit = (values: EditCommentFormValues) => {
        setShowModal(false);
        if (task) {
            let changedTask = {...task};
            changedTask.controller_comment = values.controller_comment;
            dispatch(TasksAction.changeCompletedTaskDataAction(changedTask, userId));
            // navigation.goBack();
        }
    }

    useEffect(() => {
        if (tasks) {
            setTask(tasks[props.taskId]);
        }
    }, [tasks]);

    const editIcon = () => (
        <Feather name="edit" size={24} color={THEME.PRIMARY_COLOR} />
    )

    return (
        <>
            <Button
                appearance={'outline'}
                style={styles.button}
                size={'medium'}
                status={''}
                accessoryLeft={editIcon}
                onPress={onOpenModal}
                activeOpacity={0.7}
            >
                Изменить
            </Button>
            <SimpleModal
                isVisible={showModal}
                onCloseModal={onCloseModal}
            >
                {task
                    ? task.counter_physical_status == 0 ? (
                            <EditCounterValueForm
                                initialValues={{
                                    counter_value: (task.current_counter_value !== null) && (task.current_counter_value.length > 0)
                                        ? task.current_counter_value[0].toString()
                                        : '',
                                    controller_comment: task.controller_comment || '',
                                    counter_value_photo: getCounterValuePhotoForForm(task)
                                }}
                                onSubmit={onCounterValueFormSubmit}
                            />
                        ) : (
                            <EditCommentForm
                                initialValues={{
                                    controller_comment: task.controller_comment || '',
                                }}
                                onSubmit={onCommentFormSubmit}
                            />
                        )
                    : (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}
            </SimpleModal>
        </>
    );
}