import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    screenWrapper: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    contentWrapper: {
        flex: 1,
        paddingTop: 10
    },
    contentHeader: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: THEME.OUTLINE_COLOR,
        paddingBottom: 10,
    },
    contentHeaderCounter: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
        fontWeight: '700',
    },
    contentHeaderSubscriber: {
        textAlign: "center",
        fontSize: 14,
        color: THEME.TEXT_COLOR,
    },
    contentHeaderAgreement: {
        textAlign: "center",
        fontSize: 14,
        color: THEME.TEXT_COLOR,
    },
    contentHeaderAddress: {
        textAlign: "left",
        fontSize: 14,
        color: THEME.TEXT_COLOR,
    },
    sectionTitle: {
        alignSelf: 'flex-start',
        paddingHorizontal: 20,
        fontSize: 16,
        color: THEME.SECONDARY_COLOR,
        fontWeight: '700',
        marginVertical: 10,
    },
    contentBody: {
        alignItems: 'center',
        paddingTop: 10,
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
    spinnerBackground: {
        width: '100%',
        height: '110%',
        position: 'absolute',
        top: 0,
        left: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
    },
    commentTab: {
        width: '100%',
        borderWidth: 1,
        borderColor: THEME.WARNING_COLOR,
        borderRadius: 6,
        marginTop: 10,
    },
    commentTitleWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    commentTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
    },
    commentTitle: {
        paddingLeft: 5,
        fontSize: 15,
        fontWeight: '700',
        color: THEME.WARNING_COLOR,
    },
    commentTextContainer: {
        borderTopWidth: 1,
        borderTopColor: THEME.WARNING_COLOR,
    },
    commentText: {
        color: THEME.TEXT_COLOR,
        padding: 10,
        fontSize: 15,
    },
});