import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
    listHeader: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        flexDirection: 'row',
        width: '100%',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        alignContent: 'center'
    },
    listHeaderText: {
        color: THEME.TEXT_COLOR,
        display: 'flex',
        fontSize: 15,
        fontWeight: '700',
        paddingLeft: 10,
        textAlign: 'left',
        textAlignVertical: 'center'
    },
    addressHeader: {
        width: '80%',
    },
    quantityHeader: {
        width: '20%'
    },
});