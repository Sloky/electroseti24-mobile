import React, {useEffect, useLayoutEffect, useState} from "react";
import {ActivityIndicator, StyleSheet, Text, View, Dimensions, Keyboard} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import AddressList from "./AddressList";
import SubscriberTypeModal from "./SubscriberTypeModal";
import {useDispatch, useSelector} from "react-redux";
import {getAddressList} from "../../helpers";
import {ScreenProps} from "../../types/screenProps";
import {TaskData} from "../../interfaces";
import {globalStateInterface} from "../../store/combinedState";
import {TaskMenu} from "../../components/TaskMenu/TaskMenu";
import Footer from "../../components/Footer/Footer";
import {styles} from "./AddressListScreenStyles";
import SearchComponent from "../../components/SearchComponent/SearchComponent";

interface filterInterface {
    subscriberTypes: number[]
    search?: string
}

function applyTaskFilter(tasksData: TaskData[], filter: filterInterface) {
    let tasks = tasksData.filter(
        task => filter.subscriberTypes.find(value => task.subscriber_type == value) !== undefined
    );
    return tasks;
}

function applyTaskSearches(tasksData: TaskData[], searchValue: string) {
    let reqExp = new RegExp(searchValue, 'i');
    let tasks = tasksData.filter(
        task =>
            reqExp.test(task.subscriber_title) ||
            reqExp.test(task.counter_number) ||
            reqExp.test(task.agreement_number) ||
            reqExp.test(task.locality) ||
            reqExp.test(task.street) ||
            reqExp.test(task.house_number) ||
            reqExp.test(task.temporary_address)
    );
    return tasks;
}

const screenProps = {
    button1: {
        title: 'Задачи к выполнению',
        link: 'AddressListScreen',
        active: true
    },
    button2: {
        title: 'Выполненные задачи',
        link: 'ExecutedTaskListScreen',
    },
}

export const AddressListScreen = ({navigation}: ScreenProps) => {
    const [loader, setLoader] = useState<boolean>(false);
    const [filter, setFilter] = useState<filterInterface>({subscriberTypes: [0, 1, 2]});
    const [filteredTasks, setFilteredTasks] = useState<TaskData[]>([]);
    const [backdrop, setBackdrop] = useState<boolean>(false);
    const [keyboardStatus, setKeyboardStatus] = useState<boolean | undefined>(undefined);

    const dispatch = useDispatch();
    const window = Dimensions.get("window");

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    const tasksData = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks);

    const onSubscriberTypeChange = (types: number[]) => {
        setFilter({subscriberTypes: types});
    };

    const onChangeSearchValue = (searchValue: string) => {
        if (tasksData) {
            setFilteredTasks(applyTaskSearches(Object.values(tasksData), searchValue));
        }
    };

    useEffect(() => {
        if (tasksData) {
            setFilteredTasks(applyTaskFilter(Object.values(tasksData), filter));
        }
    }, [filter, tasksData]);

    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Список задач',
            headerShown: true,
            headerStyle: {
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerLeft: () => (<View/>),
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation, backdrop]);

    return (
        <>
            {!keyboardStatus && <TaskMenu button1={screenProps.button1} button2={screenProps.button2}/>}
            <View style={styles.center}>
                <SubscriberTypeModal
                    onSubscriberTypeChange={onSubscriberTypeChange}
                    initialTypes={filter.subscriberTypes}
                    setBackdrop={setBackdrop}
                />
                <View style={styles.listHeader}>
                    <Text style={{...styles.listHeaderText, ...styles.addressHeader}}>Выберите адрес</Text>
                    <Text style={{...styles.listHeaderText, ...styles.quantityHeader}}>Узлы учёта</Text>
                </View>
                {!loader ? (
                    <AddressList
                        listData={getAddressList(filteredTasks)}
                    />
                ) : (
                    <View style={styles.spinner}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR}/>
                    </View>
                )}
                <SearchComponent onChangeSearchValue={onChangeSearchValue}/>
            </View>
            {!keyboardStatus && <Footer activeBtn={'Tasks'}/>}
            <View style={{
                position: 'absolute',
                height: backdrop ? window.height : 0,
                width: '100%',
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : 'transparent'
            }}/>
        </>
    );
};
