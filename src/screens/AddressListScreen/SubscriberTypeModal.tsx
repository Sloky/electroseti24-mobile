import React, {useEffect, useState, Dispatch, SetStateAction} from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {THEME} from "../../theme";
import {Entypo, Feather} from "@expo/vector-icons";
import {Button, Modal, Toggle, useTheme, CheckBox} from "@ui-kitten/components";
import SimpleModal from "../../components/SimpleModal/SimpleModal";

interface SubscriberTypeModalProps {
    onSubscriberTypeChange: any
    initialTypes: number[]
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

interface checkboxInterface {
    title: string
    subscriber_type: number
}

const checkboxes: checkboxInterface[] = [
    {
        title: 'Физические лица',
        subscriber_type: 0,
    },
    {
        title: 'Юридические лица',
        subscriber_type: 1,
    },
    {
        title: 'Центральное управление',
        subscriber_type: 2,
    },
];

const subscriberTypeTitles: any = {
    '0': 'ФЛ',
    '1': 'ЮЛ',
    '2': 'ЦУ'
};

function uniq(arr: number[]) {
    return Array.from(new Set(arr));
}

function arrRemoveItem(item: number, arr: number[]) {
    return arr.filter(value => item != value);
}

export default function SubscriberTypeModal(props: SubscriberTypeModalProps) {
    const [modalVisible, setModalVisible] = useState<boolean>(false);
    const [modalButtonTitle, setModalButtonTitle] = useState<string>('');
    const [checkedItems, setCheckedItems] = useState<number[]>(props.initialTypes);

    // const theme = useTheme();

    const onCheckBoxChange = (value: number) => {
        if (checkedItems.find((val) => val == value) !== undefined) {
            setCheckedItems(arrRemoveItem(value, checkedItems));
        } else {
            let uniqueArr = uniq([...checkedItems, value]);
            setCheckedItems(uniqueArr);
        }
    };

    const onModalSubmit = () => {
        let modalTitlesArr = checkedItems.map(value => subscriberTypeTitles[value.toString()]);
        let modalTitle = modalTitlesArr.join(', ');
        setModalButtonTitle(modalTitle);
        props.onSubscriberTypeChange(checkedItems);
    };

    const onOpenModal = () => {
        setCheckedItems(props.initialTypes);
        props.setBackdrop(true);
        setModalVisible(true);
    };

    const onCloseModal = () => {
        onModalSubmit();
        props.setBackdrop(false);
        setModalVisible(false);
    };

    useEffect(() => {
        let modalTitlesArr = checkedItems.map(value => subscriberTypeTitles[value.toString()]);
        let modalTitle = modalTitlesArr.join(', ');
        setModalButtonTitle(modalTitle);
    }, []);

    return (
        <View style={{width: '100%', padding: 10}}>
            <SimpleModal
                isVisible={modalVisible}
                onCloseModal={onCloseModal}
            >
                <View>
                    <Text style={styles.title}>Тип абонента</Text>
                    <Text style={styles.subtitle}>Выберите один или несколько типов абонента</Text>
                    {checkboxes.map(item => {
                        let checked = checkedItems.find((value) => value == item.subscriber_type) !== undefined;
                        return (
                            <TouchableOpacity
                                key={item.subscriber_type}
                                onPress={(checked) => {
                                    onCheckBoxChange(item.subscriber_type)
                                }}
                            >
                                <View
                                    style={checked ? {...styles.checkboxContainer, ...styles.checkedContainer} : {...styles.checkboxContainer}}>
                                    <Text style={checked ? {...styles.checkboxLabel, ...styles.checkedLabel} : {...styles.checkboxLabel}}>{item.title}</Text>
                                    <View style={styles.checkbox}>
                                        <CheckBox
                                            checked={checked}
                                            onChange={(checked) => {
                                                onCheckBoxChange(item.subscriber_type)
                                            }}
                                        />
                                    </View>

                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </SimpleModal>

            <View style={styles.titleContainer}>
                <Text style={styles.title}>Выберите тип абонента</Text>
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={onOpenModal}
            >
                <Text style={styles.text}>{modalButtonTitle ? '' + modalButtonTitle : ''}</Text>
                <Entypo name="chevron-down" size={30} color={THEME.SECONDARY_COLOR}/>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        marginHorizontal: 10,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: THEME.OUTLINE_COLOR,
        backgroundColor: THEME.BACKGROUND_COLOR
    },
    titleContainer: {
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    title: {
        color: THEME.TEXT_COLOR,
        fontSize: 15,
        fontWeight: "700",
    },
    text: {
        color: THEME.PRIMARY_COLOR,
        fontSize: 14,
    },
    subtitle: {
        fontSize: 15,
        marginBottom: 10,
    },
    checkboxContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderWidth: 1,
        borderColor: THEME.OUTLINE_COLOR,
        borderRadius: 6,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        marginBottom: 10,
    },
    checkboxLabel: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
    checkedContainer: {
        borderColor: THEME.PRIMARY_COLOR,
    },
    checkedLabel: {
        color: THEME.PRIMARY_COLOR,
    },
    checkbox: {
        padding: 5,
        borderWidth: 1,
        borderRadius: 6,
        borderColor: THEME.PRIMARY_COLOR,
    }
});