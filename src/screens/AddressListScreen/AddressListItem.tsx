import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {THEME} from "../../theme";
import {ScreenProps} from "../../types/screenProps";
import { useNavigation } from '@react-navigation/native';

interface AddressListItemProps {
    address: string
    countersQuantity: number
    identifiers: string[]
}

export default function AddressListItem(props: AddressListItemProps) {
    const navigation = useNavigation();

    const onItemPress = () => {
        navigation.navigate('SubscribersList', {address: props.address, tasks: props.identifiers});
    };

    return (
        <TouchableOpacity style={styles.wrapper} onPress={onItemPress}>
            <Text style={styles.address}>{props.address}</Text>
            <Text style={styles.quantity}>{props.countersQuantity}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: THEME.BACKGROUND_COLOR,
        paddingVertical: 15,
        paddingHorizontal: 20,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: THEME.OUTLINE_COLOR,
    },
    address: {
        fontSize: 15,
        width: '80%',
        color: THEME.TEXT_COLOR,
    },
    quantity: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
        width: '20%',
        textAlign: 'center',
        textAlignVertical: 'center'
    }
});