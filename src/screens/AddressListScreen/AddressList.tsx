import React, {useState} from "react";
import {FlatList, StyleSheet, Text, View} from "react-native";
import AddressListItem from "./AddressListItem";
import {AddressListItemInterface, TaskData} from "../../interfaces";
import {getTasksListRequest, getTasksProgressRequest} from "../../requests";
import {TasksAction} from "../../store/task/actions";
import {getTasksCollection, getUnchangeableTasks} from "../../helpers";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import {ProfileProgressAction} from "../../store/profileProgress/actions";

interface AddressListProps {
    listData: AddressListItemInterface[]
}

export default function AddressList(props: AddressListProps) {
    const [refreshing, setRefreshing] = useState(false);

    const dispatch = useDispatch();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    const tasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks || {});
    const completedTasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.completedTasks || {});

    const onListRefresh = () => {
        setRefreshing(true);
        getTasksListRequest().then(
            result => {
                if (result.type == 'success') {
                    console.log('request success');
                    let unchangeableTasksIds = getUnchangeableTasks(Object.values({...tasks, ...completedTasks}))
                    let refreshedTasks = result.data.filter((task: TaskData) => {
                        return !unchangeableTasksIds.find((unchangeableTask: string) => unchangeableTask == task.id);
                    });
                    dispatch(TasksAction.updateTasksDataAction(getTasksCollection(refreshedTasks), userId));
                } else {
                    console.log('Task request error: ', result.data);
                }
            }
        )
        getTasksProgressRequest(userId).then(result => {
            if (result.type === 'success') {
                dispatch(ProfileProgressAction.changeProfileProgressData(result.data.data));
            } else {
                dispatch(ProfileProgressAction.changeProfileProgressData({
                    executedTaskCount: 0,
                    taskRatio: 0,
                    taskToWorkCount: 0,
                }));
            }
        }).finally(() => {
            setRefreshing(false);
        });
    }

    return (
        <>
            <FlatList
                style={styles.flatList}
                data={props.listData}
                keyExtractor={item => item.address}
                refreshing={refreshing}
                onRefresh={onListRefresh}
                ListEmptyComponent={
                    <View style={{flex: 1, alignItems: 'center', paddingTop: 20}}>
                        <Text>Нет данных</Text>
                    </View>
                }
                renderItem={value => {
                    return (
                        <View>
                            <AddressListItem
                                address={value.item.address}
                                countersQuantity={value.item.counterQuantity}
                                identifiers={value.item.identifiers}
                            />
                        </View>
                    );
                }}
            />
        </>
    );
};

const styles = StyleSheet.create({
    flatList: {
        width: '100%',
        // flex: 1
    },
});