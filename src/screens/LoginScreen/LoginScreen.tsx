import React, {useLayoutEffect, useState} from "react";
import {
    View,
    Image,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {ScreenProps} from "../../types/screenProps";
import {useDispatch, useSelector} from 'react-redux';
import {THEME} from "../../theme";
import {Button, Input, Spinner} from "@ui-kitten/components";
import {loginRequest} from "../../requests";
import {AppAction} from "../../store/app/actions";
import {Ionicons} from "@expo/vector-icons";
import {styles} from "./LoginScreenStyles";

export const LoginScreen = ({navigation}: ScreenProps) => {
    const [username, setUsername] = useState<string>();
    const [password, setPassword] = useState<string>();
    const [secureTextEntry, setSecureTextEntry] = useState(true);

    const [borderColor, setBorderColor] = useState(THEME.SECONDARY_COLOR);

    const dispatch = useDispatch();

    const onUsernameChange = (val: string) => {
        setUsername(val);
    };

    const onPasswordChange = (val: string) => {
        setPassword(val);
    };

    const onLoginSubmit = async () => {
        dispatch(AppAction.showLoader());
        await loginRequest(username, password).then(
            result => {
                if (result.type == 'success') {
                    dispatch(AppAction.loginSuccessAction(result.data.token, true, result.data.user));
                    dispatch(AppAction.hideLoader());
                } else {
                    dispatch(AppAction.loginErrorAction(result));
                    dispatch(AppAction.hideLoader());
                    setBorderColor('red');
                }
            }
        );
    };

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const eyeIcon = () => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Ionicons color={THEME.PRIMARY_COLOR} name={secureTextEntry ? 'eye-off-outline' : 'eye-outline'} size={20}/>
        </TouchableWithoutFeedback>
    );

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerShown: false,
        };
        navigation.setOptions(navigationOptions);
    }, [navigation]);


    return (
        <>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={styles.wrapper}>
                    <Image
                        style={styles.image}
                        source={require('../../../assets/logo-icon.png')}
                    />
                    <Input
                        style={{...styles.input, borderColor: borderColor}}
                        value={username}
                        placeholder={'Введите имя пользователя'}
                        onChangeText={value => onUsernameChange(value)}
                    />
                    <Input
                        style={{...styles.input, borderColor: borderColor}}
                        value={password}
                        placeholder={'Введите пароль'}
                        accessoryRight={eyeIcon}
                        secureTextEntry={secureTextEntry}
                        onChangeText={value => onPasswordChange(value)}
                    />
                    <Button
                        style={styles.button}
                        onPress={onLoginSubmit}
                        size={'medium'}
                        appearance='filled'
                        activeOpacity={0.7}
                    >
                        Войти
                    </Button>
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

const navigationOptions: StackNavigationOptions = {
    headerShown: false,
};

LoginScreen.navigationOptions = navigationOptions;