import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    image: {
        width: 200,
        height: 200
    },
    input: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        marginBottom: 5,
        width: '90%'
    },
    button: {
        marginTop: 5,
        width: '90%',
    },
    spinnerBackground: {
        width: '100%',
        height: '110%',
        position: 'absolute',
        top: 0,
        left: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
    },
});