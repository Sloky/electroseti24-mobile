import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    center: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    item: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: THEME.PRIMARY_COLOR,
        borderRadius: 10,
        width: '100%',
        paddingVertical: 10,
        marginBottom: 20
    },
    itemText: {
        color: THEME.PRIMARY_COLOR,
        fontSize: 18,
    },
    itemWrapper: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20,
    },
    titleWrapper: {
        alignItems: 'flex-start',
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    cardWrapper: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
        width: '100%',
        borderWidth: 1,
        borderColor: THEME.PRIMARY_COLOR,
        borderRadius: 6,
        padding: 10,
        marginBottom: 15,
    },
    titleStyle: {
        justifyContent: 'flex-start',
        textAlign: 'left',
        fontWeight: '700',
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
    titleItem: {
        fontSize: 15,
        marginRight: 25,
    },
    textItem: {
        fontSize: 15,
    },
    notificationIcon: {
        position: 'absolute',
        bottom: -7,
        right: -7,
        backgroundColor: THEME.BACKGROUND_COLOR,
        borderRadius: 10,
    },
    notificationText: {
        color: THEME.WARNING_COLOR,
        fontSize: 14,
    },
    subscriberButtonTitle: {
        color: THEME.TEXT_COLOR,
        fontSize: 15,
        marginBottom: 5,
    },
    subscriberButtonIcon: {
        backgroundColor: THEME.SECONDARY_COLOR,
        borderRadius: 6,
    },
    subscriberText: {
        paddingHorizontal: 20,
        width: '80%',
    },
    subscriberAgreement: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
    footer: {
        width: '100%',
    },
});