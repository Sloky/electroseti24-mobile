import React, {useEffect, useLayoutEffect, useState} from "react";
import {ActivityIndicator, Button, Keyboard, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {THEME} from "../../theme";
import AgreementListItem from "../AgreementListScreen/AgreementListItem";
import {useNavigation, useRoute} from "@react-navigation/core";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {TaskData, TasksCollection} from "../../interfaces";
import {styles} from "./AgreementScreenStyles";
import {MaterialIcons, AntDesign} from "@expo/vector-icons";
import {useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import Footer from "../../components/Footer/Footer";
import SearchComponent from "../../components/SearchComponent/SearchComponent";

function applyTaskSearches(tasksData: TaskData[], searchValue: string) {
    let reqExp = new RegExp(searchValue, 'i');
    let tasks = tasksData.filter(
        task =>
            reqExp.test(task.counter_number)
    );
    return tasks;
}

function getTasks(tasksData: TasksCollection, taskIds: string[]) {
    let tasksArr: TaskData[] = [];
    for (let task_id in taskIds) {
        if (tasksData[taskIds[task_id]]/* && tasksData[taskIds[task_id]].status == 0*/) {
            tasksArr.push(tasksData[taskIds[task_id]]);
        }
    }
    return tasksArr;
}

export const AgreementScreen = () => {
    const [tasks, setTasks] = useState<TaskData[]>([]);
    const [keyboardStatus, setKeyboardStatus] = useState<boolean | undefined>(undefined);

    const route: any = useRoute();
    const navigation: any = useNavigation();
    const taskIds: string[] = route.params.task_ids;

    const agreement = route.params.agreement;
    const subscriber = route.params.subscriber;

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasksData = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks);

    useEffect(() => {
        if (tasksData) {
            // let tasksArr: TaskData[] = [];
            // for (let task_id in taskIds) {
            //     if (tasksData[taskIds[task_id]]/* && tasksData[taskIds[task_id]].status == 0*/) {
            //         tasksArr.push(tasksData[taskIds[task_id]]);
            //     }
            // }
            // setTasks(tasksArr);
            setTasks(getTasks(tasksData, taskIds))
        }
    }, [tasksData]);

    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    const onChangeSearchValue = (searchValue: string) => {
        if (tasksData) {
            // setTasks(applyTaskSearches(Object.values(tasksData), searchValue));
            setTasks(applyTaskSearches(getTasks(tasksData, taskIds), searchValue));
        }
    };

    const onSubscriberButtonPress = () => {
        navigation.navigate('SubscriberScreen', {task_ids: taskIds});
    }

    const onCounterButtonPress = (id: string) => {
        navigation.navigate('TaskScreen', {task_id: id, isCompletedTask: false, taskCount: tasks.length});
    }

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Абонент',
            headerStyle: {
                backgroundColor: THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: THEME.SECONDARY_COLOR,
            headerBackTitle: ' ',
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation]);

    return (
        <>
            <View style={styles.center}>
                <View style={styles.titleWrapper}>
                    <Text style={styles.titleStyle}>Карточка абонента</Text>
                </View>
                <View style={{paddingHorizontal: 20}}>
                    <TouchableOpacity
                        style={styles.cardWrapper}
                        onPress={onSubscriberButtonPress}
                    >
                        <View style={styles.subscriberButtonIcon}>
                            <MaterialIcons name="person-outline" size={56} color={THEME.BACKGROUND_COLOR}/>
                            {tasks[0] && tasks[0].subscriber_type === 0
                            && (!tasks[0].rooms_count || !tasks[0].lodgers_count)
                                ? <AntDesign style={styles.notificationIcon} name="exclamationcircle" size={18} color={THEME.WARNING_COLOR}/>
                                : null
                            }
                        </View>
                        <View style={styles.subscriberText}>
                            <Text style={styles.subscriberButtonTitle}>{subscriber}</Text>
                            <View style={styles.subscriberAgreement}>
                                <Text style={styles.titleItem}>Договор</Text>
                                <Text style={styles.textItem}>{agreement}</Text>
                            </View>
                            {tasks[0] && tasks[0].subscriber_type === 0
                            && (!tasks[0].rooms_count || !tasks[0].lodgers_count)
                                ? <Text style={styles.notificationText}>Необходимо заполнить данные</Text>
                                : null
                            }
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.titleWrapper}>
                    <Text style={styles.titleStyle}>Выберите узел учёта</Text>
                </View>
                <ScrollView
                    style={styles.itemWrapper}
                    contentContainerStyle={{alignItems: 'center'}}
                >
                    {tasks.length > 0 ? tasks.map(task =>
                            <TouchableOpacity
                                style={styles.item}
                                key={task.counter_number + '_' + task.period}
                                onPress={() => onCounterButtonPress(task.id)}
                            >
                                <Text style={styles.itemText}>{task.counter_number}</Text>
                            </TouchableOpacity>
                        )
                        :
                        <View style={styles.spinner}>
                            <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR}/>
                        </View>
                    }
                </ScrollView>
                <SearchComponent onChangeSearchValue={onChangeSearchValue}/>
            </View>
            {!keyboardStatus && <Footer activeBtn={'Tasks'}/>}
        </>
    )
};