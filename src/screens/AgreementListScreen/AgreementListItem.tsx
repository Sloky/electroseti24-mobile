import React from "react";
import {StyleSheet, FlatList, Text, View, TouchableOpacity} from "react-native";
import {THEME} from "../../theme";
import {AgreementCardInterface} from "./AgreementListScreen";
import {useNavigation} from "@react-navigation/core";

interface SubscriberCardProps {
    agreementsData: AgreementCardInterface[]
}

function AgreementListItem(props: SubscriberCardProps) {
    // console.log(props.agreementsData);
    const navigation = useNavigation();

    return (
        <View style={{flex: 1}}>
        <FlatList
            data={props.agreementsData}
            keyExtractor={item => item.agreement_number}
            renderItem={value => {
                const onItemPress = () => {
                    navigation.navigate('AgreementScreen', {
                        agreement: value.item.agreement_number,
                        subscriber: value.item.subscriber_title,
                        task_ids: value.item.tasks_ids
                    });
                };
                return (
                    <TouchableOpacity
                        key={value.item.agreement_number}
                        style={styles.agreementCard}
                        onPress={onItemPress}
                    >
                        <Text style={styles.subscriberTitle}>{value.item.subscriber_title}</Text>
                        <Text style={styles.text}>Договор: {value.item.agreement_number}</Text>
                        <Text style={styles.text}>Телефон: {value.item.subscriber_phone}</Text>
                    </TouchableOpacity>
                );
            }}
        />
        </View>
    );
}

export default AgreementListItem;

const styles = StyleSheet.create({
    agreementCard: {
        padding: 10,
        marginVertical: 10,
        marginHorizontal: 20,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: THEME.PRIMARY_COLOR,

    },
    subscriberTitle: {
        color: THEME.PRIMARY_COLOR,
        fontSize: 15,
    },
    text: {
        fontSize: 14,
        color: THEME.TEXT_COLOR,
    }
});