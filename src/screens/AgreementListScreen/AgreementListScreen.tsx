import React, {useEffect, useLayoutEffect, useState} from "react";
import {Text, View, StyleSheet, Keyboard} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import Store from "../../store/store";
import {IDictionary, TasksCollection} from "../../interfaces";
import AgreementListItem from "./AgreementListItem";
import {TopRightMenu} from "../../components/TopRightMenu/TopRightMenu";
import Footer from "../../components/Footer/Footer";
import {useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import SearchComponent from "../../components/SearchComponent/SearchComponent";

export interface AgreementCardInterface {
    agreement_number: string
    subscriber_title: string
    subscriber_phone: string
    tasks_ids: string[]
}

const getAgreementCardData = (tasksIDs: string[], tasks: TasksCollection) => {
    let agreementCardData: IDictionary<AgreementCardInterface> = {};
    try {
        let filteredTasks = tasksIDs.map(taskId => tasks[taskId]);
        filteredTasks = filteredTasks.filter(task => task !== undefined);

        filteredTasks.map((task) => {
            if (!agreementCardData.hasOwnProperty(task.agreement_number)) {
                agreementCardData[task.agreement_number] = {
                    agreement_number: task.agreement_number,
                    subscriber_title: task.subscriber_title,
                    subscriber_phone: task.subscriber_phones[0] || 'Номер телефона не известен',
                    tasks_ids: [task.id]
                };
            } else {
                agreementCardData[task.agreement_number].tasks_ids.push(task.id);
            }
        });
    } catch (e) {
        console.log(e);
    }
    return Object.values(agreementCardData);
};

function applyAgreementSearches(agreementData: AgreementCardInterface[], searchValue: string) {
    let reqExp = new RegExp(searchValue, 'i');
    let agreements = agreementData.filter(
        agreement =>
            reqExp.test(agreement.subscriber_title) ||
            reqExp.test(agreement.agreement_number)
    );
    return agreements;
}

export const AgreementListScreen = ({navigation, route}: any) => {
    const [agreementCardData, setAgreementCardData] = useState<AgreementCardInterface[]>([]);
    const [keyboardStatus, setKeyboardStatus] = useState<boolean | undefined>(undefined);

    const address = route.params.address;
    const tasksIDs = route.params.tasks;

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasksData = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks);

    const onChangeSearchValue = (searchValue: string) => {
        if (tasksData) {
            setAgreementCardData(applyAgreementSearches(getAgreementCardData(tasksIDs, tasksData), searchValue));
        }
    };

    useEffect(() => {
        if (tasksData) {
            setAgreementCardData(getAgreementCardData(tasksIDs, tasksData));
        }
    }, [tasksData]);

    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
            setKeyboardStatus(true);
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            setKeyboardStatus(false);
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Список абонентов',
            headerStyle: {
                backgroundColor: THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: THEME.SECONDARY_COLOR,
            headerBackTitle: ' ',
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation]);

    return (
        <>
            <View style={styles.center}>
                <View style={styles.addressWrapper}>
                    <Text style={styles.title}>Адрес</Text>
                    <Text style={styles.address}>{address}</Text>
                    <Text style={styles.title}>Выберите абонента</Text>
                </View>
                <AgreementListItem agreementsData={agreementCardData}/>
                <SearchComponent onChangeSearchValue={onChangeSearchValue}/>
            </View>
            {!keyboardStatus && <Footer activeBtn={'Tasks'}/>}
        </>
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    addressWrapper: {
        marginTop: 10,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 15,
        fontWeight: '700',
        color: THEME.TEXT_COLOR,
        marginBottom: 5,
    },
    address: {
        marginBottom: 15,
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
});