import {StyleSheet} from "react-native";
import {THEME} from "../../../../theme";

export const styles = StyleSheet.create({
    title: {
        textAlign: 'center',
        marginBottom: 10,
        fontSize: 16,
        color: THEME.TEXT_COLOR,
        fontWeight: '700',
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    inputLabel: {
        width: '50%',
        fontSize: 15,
        color: THEME.TEXT_COLOR,
        paddingRight: 5,
    },
    formInput: {
        width: '50%',
        paddingLeft: 5,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    // formBtn: {
    //     backgroundColor: NEW_THEME.PRIMARY_COLOR,
    //     borderColor: NEW_THEME.PRIMARY_COLOR,
    // }
});