import React from "react";
import {Keyboard, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./EditSubscriberFormStyles";
import {EditCounterFormValues} from "../../../TaskScreen/EditCounterModalForm/EditCounterForm/EditCounterForm";
import * as Yup from "yup";
import {Button, Input, Toggle} from "@ui-kitten/components";
import {PhotoPicker} from "../../../../components/PhotoPicker/PhotoPicker";
import {Formik} from "formik";

export interface EditSubscriberFormValues {
    // surname: string
    // name: string
    // patronymic: boolean
    // locality: string
    lodgers_count: string
    rooms_count: string
}

interface EditSubscriberFormProps {
    initialValues: EditSubscriberFormValues
    onSubmit(values: EditSubscriberFormValues): void
}

export function EditSubscriberForm(props: EditSubscriberFormProps) {
    const validationSchema = Yup.object().shape({
        lodgers_count: Yup.number(),
        rooms_count: Yup.number(),
    });
    return (
        <TouchableOpacity onPress={Keyboard.dismiss} activeOpacity={1}>
            <Text style={styles.title}>Изменение данных</Text>
            <Formik
                initialValues={props.initialValues}
                validationSchema={validationSchema}

                onSubmit={(values) => {
                    props.onSubmit(values);
                }}
            >
                {({
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      dirty,
                      isValid,
                      errors,
                      values
                  }) => (
                    <>
                        <View style={styles.inputContainer}>
                            <Text style={styles.inputLabel}>Количество комнат</Text>
                            <Input
                                style={styles.formInput}
                                onChangeText={handleChange('rooms_count')}
                                status={errors.rooms_count ? 'danger' : 'basic'}
                                value={values.rooms_count.toString()}
                                keyboardType={'decimal-pad'}
                            />
                        </View>

                        <View style={styles.inputContainer}>
                            <Text style={styles.inputLabel}>Количество проживающих</Text>
                            <Input
                                style={styles.formInput}
                                onChangeText={handleChange('lodgers_count')}
                                status={errors.lodgers_count ? 'danger' : 'basic'}
                                value={values.lodgers_count.toString()}
                                keyboardType={'decimal-pad'}
                            />
                        </View>


                        <Button
                            onPress={() => handleSubmit()}
                            disabled={!(isValid && dirty)}
                            status={'primary'}
                            size={'medium'}
                            activeOpacity={0.7}
                        >
                            Сохранить
                        </Button>
                    </>
                )}
            </Formik>
        </TouchableOpacity>
    );
}