import React, {Dispatch, SetStateAction ,useEffect, useState} from "react";
import {Button} from "@ui-kitten/components";
import SimpleModal from "../../../components/SimpleModal/SimpleModal";
import {EditCounterForm} from "../../TaskScreen/EditCounterModalForm/EditCounterForm/EditCounterForm";
import {ActivityIndicator, Text, View} from "react-native";
import {THEME} from "../../../theme";
import {TaskData} from "../../../interfaces";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../../store/combinedState";
import {styles} from "./EditSubscriberModalFormStyles";
import {EditSubscriberForm, EditSubscriberFormValues} from "./EditSubscriberForm/EditSubscriberForm";
import {getObjCompDifField} from "../../../helpers";
import {TasksAction} from "../../../store/task/actions";

interface EditSubscriberModalFormProps {
    taskId: string
    tasks_ids: string[]
    setBackdrop: Dispatch<SetStateAction<boolean>>
}

export function EditSubscriberModalForm(props: EditSubscriberModalFormProps) {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [task, setTask] = useState<TaskData>();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => {
        return state.tasks[userId].tasks;
    });

    const dispatch = useDispatch();

    const onOpenModal = () => {
        props.setBackdrop(true);
        setShowModal(true);
    };

    const onCloseModal = () => {
        props.setBackdrop(false);
        setShowModal(false);
    };

    const onSubscriberFormSubmit = (values: EditSubscriberFormValues) => {
        props.setBackdrop(false);
        setShowModal(false);

        if (task) {
            let changedFields = getObjCompDifField(
                {
                    rooms_count: task.rooms_count,
                    lodgers_count: task.lodgers_count,
                },
                {
                    rooms_count: values.rooms_count ? Number(values.rooms_count) : undefined,
                    lodgers_count: values.lodgers_count ? Number(values.lodgers_count) : undefined,
                }
            );
            props.tasks_ids.map((task_id: string) => {
                if (tasks) {
                    let changedTask = {...tasks[task_id]}
                    changedTask.rooms_count = values.rooms_count ? Number(values.rooms_count) : undefined;
                    changedTask.lodgers_count = values.lodgers_count ? Number(values.lodgers_count) : undefined;
                    changedTask.changed_fields.push(...changedFields);
                    dispatch(TasksAction.changeTaskDataAction(changedTask, userId));
                }
            });
        }
    };

    useEffect(() => {
        if (tasks) {
            setTask(tasks[props.taskId]);
        }
    }, [tasks]);

    return (
        <>
            <Button
                style={styles.button}
                size={'medium'}
                status={''}
                onPress={onOpenModal}
                activeOpacity={0.7}
            >
                Изменить
            </Button>
            <SimpleModal
                isVisible={showModal}
                onCloseModal={onCloseModal}
            >
                {task ? (
                    <EditSubscriberForm
                        initialValues={{
                            rooms_count: task.rooms_count ? task.rooms_count.toString() : '',
                            lodgers_count: task.lodgers_count ? task.lodgers_count.toString() : ''
                        }}
                        onSubmit={onSubscriberFormSubmit}
                    />
                ) : (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR} />
                    </View>
                )}
            </SimpleModal>
        </>
    );
}