import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    screenWrapper: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    subscriberCard: {
        flex: 1,
    },
    subscriberCardItem: {
        display: "flex",
        flexDirection: 'row',
        backgroundColor: THEME.BACKGROUND_COLOR,
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: THEME.OUTLINE_COLOR,
    },
    cardItemTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        paddingHorizontal: 20,
        color: THEME.TEXT_COLOR,
    },
    cardItemValue: {
        flex: 1,
        fontSize: 15,
        color: THEME.TEXT_COLOR,
        width: '100%',
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
});