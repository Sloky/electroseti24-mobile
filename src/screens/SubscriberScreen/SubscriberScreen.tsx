import React, {useEffect, useLayoutEffect, useState} from "react";
import {ActivityIndicator, Dimensions, Text, TouchableOpacity, View} from "react-native";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import {useNavigation, useRoute} from "@react-navigation/core";
import {styles} from "./SubscriberScreenStyles";
import {useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import {TaskData} from "../../interfaces";
import {Linking} from 'react-native';
import {EditSubscriberModalForm} from "./EditSubscriberModalForm/EditSubscriberModalForm";
import Footer from "../../components/Footer/Footer";

function SubscriberCardItem(props: {title: string, value: string}) {
    return (
        <View style={styles.subscriberCardItem}>
            <Text style={styles.cardItemTitle}>{props.title}: </Text>
            <Text style={styles.cardItemValue}>{props.value}</Text>
        </View>
    );
}

export function SubscriberScreen() {
    const [task, setTask] = useState<TaskData>();
    const [backdrop, setBackdrop] = useState<boolean>(false);

    const route: any = useRoute();
    const task_ids = route.params.task_ids;
    const task_id = task_ids[0];

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    let tasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks);

    const navigation = useNavigation();
    const window = Dimensions.get("window");

    const onPhonePress = (phoneNumber: string|null) => {
        if (phoneNumber) {
            Linking.openURL(`tel:${phoneNumber}`);
        }
    };

    useEffect(() => {
        if (tasks) {
            setTask(tasks[task_id]);
        }
    }, [tasks]);

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Карточка абонента',
            headerStyle: {
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: backdrop ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
            headerBackTitle: ' ',
            headerRight: () => (<View/>)
        };
        navigation.setOptions(navigationOptions);
    }, [navigation, backdrop]);

    return (
        <>
            <View style={styles.screenWrapper}>
                <View style={styles.subscriberCard}>
                    {task ? (
                        <>
                            <SubscriberCardItem title={'Абонент'} value={task.subscriber_title}/>
                            <SubscriberCardItem title={'Договор'} value={task.agreement_number}/>
                            <SubscriberCardItem title={'Населенный пункт'} value={task.locality}/>
                            <SubscriberCardItem title={'Улица'} value={task.street}/>
                            <SubscriberCardItem title={'Дом'} value={task.house_number}/>
                            {task.subscriber_type == 0 &&
                            <>
                                <SubscriberCardItem title={'Подъезд'} value={task.porch_number}/>
                                <SubscriberCardItem title={'Квартира'} value={task.apartment_number}/>
                                <SubscriberCardItem
                                    title={'Количество комнат'}
                                    value={task.rooms_count ? task.rooms_count.toString() : 'Неизвестно'}
                                />
                                <SubscriberCardItem
                                    title={'Количество проживающих'}
                                    value={task.lodgers_count ? task.lodgers_count.toString() : 'Неизвестно'}
                                />
                            </>
                            }
                            <View style={styles.subscriberCardItem}>
                                <Text style={styles.cardItemTitle}>Телефон:</Text>
                                <TouchableOpacity
                                    onPress={() => onPhonePress(task.subscriber_phones.length > 0 ? task.subscriber_phones[0] : null)}
                                >
                                    <Text style={{...styles.cardItemValue, color: THEME.PRIMARY_COLOR}}>
                                        {
                                            task.subscriber_phones.length > 0 ? task.subscriber_phones[0] : 'Нет данных'
                                        }
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            {task.subscriber_type == 0 &&
                            <View style={{flex: 1, justifyContent: "flex-end"}}>
                                <EditSubscriberModalForm taskId={task.id} tasks_ids={task_ids} setBackdrop={setBackdrop}/>
                            </View>
                            }
                        </>
                    ) : (
                        <View style={styles.spinner}>
                            <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR}/>
                        </View>
                    )}
                </View>
                <Footer activeBtn={'Tasks'}/>
            </View>
            <View style={{
                position: 'absolute',
                height: backdrop ? window.height : 0,
                width: '100%',
                backgroundColor: backdrop ? THEME.LOADER_BACKGROUND_COLOR : 'transparent'
            }}/>
        </>
    );
}