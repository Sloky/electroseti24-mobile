import React, {useLayoutEffect} from "react";
import {StyleSheet, View} from "react-native";
import {THEME} from "../../theme";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {Button} from "@ui-kitten/components";
import {useNavigation} from "@react-navigation/core";

const screenProps = {
    button1: {
        title: 'Снятие показаний',
        link: 'AddressListScreen'
    },
    button2: {
        title: 'Фотофиксация сетевых объектов',
        link: 'MainsObjectScreen'
    },
}

export default function MainScreen() {
    const navigation = useNavigation();

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Функции приложения',
            headerStyle: {
                backgroundColor: THEME.PRIMARY_COLOR,
            },
            headerTitleStyle: {
                color: THEME.BACKGROUND_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerTintColor: THEME.BACKGROUND_COLOR,
        };
        navigation.setOptions(navigationOptions);
    }, [navigation]);

    return (
        <View style={styles.center}>
            <View style={styles.wrapper}>
                <View style={styles.buttonBlock}>
                    <Button
                        onPress={() => navigation.navigate(screenProps.button1.link)}
                        style={styles.button}
                        size={'giant'}
                    >
                        {screenProps.button1.title}
                    </Button>
                    <Button
                        onPress={() => navigation.navigate(screenProps.button2.link)}
                        style={styles.button}
                        size={'giant'}
                    >
                        {screenProps.button2.title}
                    </Button>
                </View>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    wrapper: {
        flex: 1
    },
    buttonBlock: {
        justifyContent: "center",
        paddingTop: 20,
    },
    button: {
        marginBottom: 20,
    },
});