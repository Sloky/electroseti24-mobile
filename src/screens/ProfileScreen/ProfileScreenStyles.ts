import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    wrapper: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20,
    },
    card: {
        marginTop: 20,
    },
    cardContent: {
        flexDirection: 'row',
    },
    text: {
        flex: 1,
        color: THEME.TEXT_COLOR,
        fontSize: 15,
        fontWeight: '500',
    },
    buttonBlock: {
        justifyContent: "center",
        paddingTop: 20,
    },
    button: {
        marginBottom: 20,
        backgroundColor: THEME.BACKGROUND_COLOR,
        color: THEME.PRIMARY_COLOR,
        width: '100%',
    },
    exitButtonContainer: {
        flex: 1,
        justifyContent: "flex-end",
        paddingBottom: 20,
    },
    modalText: {
        paddingTop: 30,
        paddingBottom: 30,
        textAlign: "center",
        fontSize: 16,
        fontWeight: '500',
    },
    spinner: {
        flex: 1,
        justifyContent: 'center'
    },
    progressBar: {
        backgroundColor: THEME.OUTLINE_COLOR,
        width: '100%',
        height: 10,
        borderRadius: 10,
    },
    progressTitleContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 10,
    },
    progressTitle: {
        fontSize: 18,
        fontWeight: '500',
    },
    progressText: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});