import React, {useEffect, useLayoutEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {globalStateInterface} from "../../store/combinedState";
import {View, Text, Alert, ActivityIndicator, TouchableOpacity, Modal, Dimensions, Linking} from "react-native";
import {styles} from "./ProfileScreenStyles";
import {Button, Card} from "@ui-kitten/components";
import {useNavigation} from "@react-navigation/core";
import {Ionicons, AntDesign, MaterialIcons} from '@expo/vector-icons';
import {THEME} from "../../theme";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {AppAction} from "../../store/app/actions";
import SimpleModal from "../../components/SimpleModal/SimpleModal";
import Footer from "../../components/Footer/Footer";
import {getTasksListRequest, getTasksProgressRequest} from "../../requests";
import {TaskData} from "../../interfaces";
import {TasksAction} from "../../store/task/actions";
import {getTasksCollection, getUnchangeableTasks} from "../../helpers";
import {ProfileProgressAction} from "../../store/profileProgress/actions";

const screenProps = {
    button1: {
        title: 'Написать в поддержку',
        link: 'MainScreen',
    },
    button2: {
        title: 'Посмотреть инструкцию',
        link: 'MainScreen'
    },
    button3: {
        title: 'Выйти из аккаунта',
        link: 'MainScreen'
    },
}

const monthes = [
    'январе',
    'феврале',
    'марте',
    'апреле',
    'майе',
    'июне',
    'июле',
    'августе',
    'сентябре',
    'октябре',
    'ноябре',
    'декабре',
]

export function ProfileScreen() {
    const [showModal, setShowModal] = useState<boolean>(false);
    const [refreshing, setRefreshing] = useState<boolean>(false);

    const navigation = useNavigation();
    const dispatch = useDispatch();

    const userId = useSelector((state: globalStateInterface) => state.app.user?.id) || '';
    const tasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.tasks);
    const completedTasks = useSelector((state: globalStateInterface) => state.tasks[userId]?.completedTasks);
    const userData = useSelector((state: globalStateInterface) => state.app.user);
    let branchName = useSelector((state: globalStateInterface) => state.app.user?.companies?.join(',').replace(/\s/, '_'));
    const profileProgress = useSelector((state: globalStateInterface) => state.profileProgress);
    const window = Dimensions.get("window");

    useEffect(() => {
        if (userId) {
            getTasksProgressRequest(userId).then(result => {
                if (result.type === 'success') {
                    dispatch(ProfileProgressAction.changeProfileProgressData(result.data.data));
                } else {
                    dispatch(ProfileProgressAction.changeProfileProgressData({
                        executedTaskCount: 0,
                        taskRatio: 0,
                        taskToWorkCount: 0,
                    }));
                }
            })
        }
    }, [userId])

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Профиль',
            headerStyle: {
                backgroundColor: showModal ? THEME.LOADER_BACKGROUND_COLOR : THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: showModal ? THEME.TEXT_COLOR : THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
            },
        };
        navigation.setOptions(navigationOptions);
    }, [navigation, showModal]);

    const mailIcon = () => (
        <Ionicons name={'mail-outline'} size={24} color={THEME.PRIMARY_COLOR}/>
    );

    const questionIcon = () => (
        <AntDesign name="questioncircleo" size={24} color={THEME.PRIMARY_COLOR}/>
    );

    const exitIcon = () => (
        <MaterialIcons name="logout" size={24} color={THEME.PRIMARY_COLOR}/>
    );

    const refreshTasks = () => {
        setRefreshing(true);
        getTasksListRequest().then(
            result => {
                if (result.type == 'success') {
                    console.log('request success');
                    let unchangeableTasksIds = getUnchangeableTasks(Object.values({...tasks, ...completedTasks}))
                    let refreshedTasks = result.data.filter((task: TaskData) => {
                        return !unchangeableTasksIds.find((unchangeableTask: string) => unchangeableTask == task.id);
                    });
                    dispatch(TasksAction.updateTasksDataAction(getTasksCollection(refreshedTasks), userId));
                } else {
                    console.log('Task request error: ', result.data);
                }
            }
        )
        getTasksProgressRequest(userId).then(result => {
            if (result.type === 'success') {
                dispatch(ProfileProgressAction.changeProfileProgressData(result.data.data));
            } else {
                dispatch(ProfileProgressAction.changeProfileProgressData({
                    executedTaskCount: 0,
                    taskRatio: 0,
                    taskToWorkCount: 0,
                }));
            }
        }).finally(() => {
            setRefreshing(false);
        });
    };

    const onCloseModal = () => {
        setShowModal(false);
    };

    let currentMonth = new Date().getMonth()

    return (
        <>
            {!refreshing ? (
                <View style={styles.center}>
                <View style={styles.wrapper}>
                    <Card style={styles.card}>
                        <View style={styles.cardContent}>
                            <View>
                                <Ionicons name="person-outline" size={48} color={THEME.SECONDARY_COLOR}/>
                            </View>
                            <View style={{marginLeft: 10,}}>
                                <Text
                                    style={styles.text}>{(userData && userData.surname) + ' ' + (userData && userData.name) + ' ' + (userData && userData.patronymic)}</Text>
                            </View>
                        </View>
                    </Card>
                    <Card style={styles.card}>
                        <View style={styles.progressTitleContainer}>
                            <Text style={{...styles.progressTitle, marginRight: 5,}}>{`Задач в ${monthes[currentMonth]}:`}</Text>
                            <Text style={{...styles.progressTitle, fontWeight: '700',}}>{profileProgress.executedTaskCount + profileProgress.taskToWorkCount}</Text>
                        </View>
                        <View style={{...styles.progressText, marginBottom: 10,}}>
                            <Text>Выполнено</Text>
                            <Text>Осталось</Text>
                        </View>
                        <View style={{marginBottom: 10, position: 'relative',}}>
                            <View style={{...styles.progressBar,}}>
                            </View>
                            <View style={{...styles.progressBar, position: 'absolute', top: 0, left: 0, backgroundColor: THEME.PRIMARY_COLOR, width: `${profileProgress.taskRatio * 100}%`}}>
                            </View>
                        </View>
                        <View style={styles.progressText}>
                            <Text>{profileProgress.executedTaskCount}</Text>
                            <Text>{profileProgress.taskToWorkCount}</Text>
                        </View>
                    </Card>
                    <View style={styles.buttonBlock}>
                        <Button
                            onPress={() => Linking.openURL(`http://support.electroline24.ru?user_id=${userId}&branch_name=${branchName}`)}
                            style={styles.button}
                            size={'medium'}
                            appearance='outline'
                            accessoryLeft={mailIcon}
                            activeOpacity={0.7}
                        >
                            {screenProps.button1.title}
                        </Button>
                        <Button
                            onPress={() => Linking.openURL(`http://faq.electroline24.ru?user_id=${userId}&branch_name=${branchName}`)}
                            style={styles.button}
                            size={'medium'}
                            appearance='outline'
                            accessoryLeft={questionIcon}
                            activeOpacity={0.7}
                        >
                            {screenProps.button2.title}
                        </Button>
                        <Button
                            onPress={() => {
                                setShowModal(true);
                            }}
                            style={styles.button}
                            size={'medium'}
                            appearance='outline'
                            accessoryLeft={exitIcon}
                            activeOpacity={0.7}
                        >
                            {screenProps.button3.title}
                        </Button>
                    </View>
                    <View style={styles.exitButtonContainer}>
                        <Button
                            onPress={refreshTasks}
                            size={'medium'}
                            activeOpacity={0.7}
                        >
                            Обновить задачи
                        </Button>
                    </View>
                </View>
                <SimpleModal
                    isVisible={showModal}
                    onCloseModal={onCloseModal}
                >
                    <View>
                        <Text style={styles.modalText}>Выйти из аккаунта ?</Text>
                        <Button activeOpacity={0.7} style={{width: '100%'}} size={'medium'} onPress={() => dispatch(AppAction.clearAuthState())}>Выйти</Button>
                    </View>
                </SimpleModal>
            </View>
            ) : (
                <View style={styles.spinner}>
                    <ActivityIndicator size="large" color={THEME.PRIMARY_COLOR}/>
                </View>
            )}
            <Footer activeBtn={'Profile'}/>
            <View style={{
                position: 'absolute',
                height: showModal ? window.height : 0,
                width: '100%',
                backgroundColor: showModal ? THEME.LOADER_BACKGROUND_COLOR : 'transparent'
            }}/>
        </>
    )
}