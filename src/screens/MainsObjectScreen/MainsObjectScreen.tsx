import React, {useEffect, useLayoutEffect, useState} from "react";
import {
    ActivityIndicator,
    Alert,
    FlatList,
    Image,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    ImageSourcePropType,
    Modal,
} from "react-native";
import {styles} from "./MainsObjectScreenStyles";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {THEME} from "../../theme";
import {Button, Card} from "@ui-kitten/components";
import {PhotoPicker} from "../../components/PhotoPicker/PhotoPicker";
import * as FileSystem from "expo-file-system";
import {FileSystemUploadType} from "expo-file-system";
import {useDispatch, useSelector} from "react-redux";
import {MainsObjectAction} from "../../store/mainsObect/actions";
import {globalStateInterface} from "../../store/combinedState";
import {MainsObjectCollection} from "../../store/mainsObect/state";
import * as Location from 'expo-location';
import {LocationAccuracy} from 'expo-location';
import {ProgressBar} from "../../components/ProgressBar/ProgressBar";
import Footer from "../../components/Footer/Footer";
import {AppAction} from "../../store/app/actions";
import {FontAwesome} from "@expo/vector-icons";
import ImageViewer from "react-native-image-zoom-viewer";
import {getCurrentPosition} from "../../helpers";

const getMainObjectsPhotos = (mainsObject: MainsObjectCollection): string[] => {
    let mainsObjectsArr = Object.values(mainsObject);
    return mainsObjectsArr.map(value => value.uri).reverse();
}

interface convertedPhotos {
    url: string
    props?: any
}

const convertPhotos = (photos: string[]): convertedPhotos[] => {
    return photos.map(item => ({url: item}))
}

export function MainsObjectScreen({navigation}: any) {
    const [photos, setPhotos] = useState<string[]>([]);
    const [error, setError] = useState<boolean>(false);
    const [showProgBar, setShowProgBar] = useState<boolean>(false);
    const [progress, setProgress] = useState<{ percent: number, text: string }>({
        percent: 0,
        text: '0%'
    });
    const [showModal, setShowModal] = useState<{status: boolean, index: number}>({status: false, index: 0});

    // let [imageUrl, setImageUrl] = useState<any>();

    const dispatch = useDispatch();

    const screenSize = Dimensions.get('screen');

    const userFullName = useSelector((state: globalStateInterface) => {
        let user = state.app.user;
        return user?.surname + ' ' + user?.name + ' ' + user?.patronymic;
    });

    const mainsObjects = useSelector((state: globalStateInterface) =>
        state.mainsObject.mainsObjects
    );

    const savePhoto = async (photoUri: string) => {
        dispatch(AppAction.showLoader());
        const date = new Date();

        let location = await getCurrentPosition();

        const fileName = photoUri.split('/').pop();
        const newFileName = date.toISOString() + '_mains_object_' + fileName
        const newPath = FileSystem.documentDirectory + newFileName;
        try {
            await FileSystem.moveAsync({
                from: photoUri,
                to: newPath
            });
            dispatch(MainsObjectAction.changeMainsObjectData({
                uri: newPath,
                photoName: newFileName,
                dateTime: date.toISOString(),
                gps: {
                    GPSLatitude: location.coords.latitude,
                    GPSLongitude: location.coords.longitude
                },
                userName: userFullName
            }));
            dispatch(AppAction.hideLoader());
        } catch (e) {
            Alert.alert('Произошла ошибка при сохранении фотографии. Повторите попытку');
            console.log(e);
            dispatch(AppAction.hideLoader());
        }
    };

    const onSubmitPhotos = () => {
        let photos = Object.values(mainsObjects);
        if (photos?.length > 0) {
            let count = photos.length;
            let counter = 0;
            setShowProgBar(true);
            photos.map(photo => {
                let fileName = photo.uri;
                FileSystem.uploadAsync(
                    'https://api.mod4.energoline24.ru/api/media_objects ',
                    fileName,
                    {
                        fieldName: 'file',
                        httpMethod: "POST",
                        uploadType: FileSystemUploadType.MULTIPART,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                        parameters: {
                            'DateTimeOriginal': photo.dateTime,
                            'GPS[GPSLatitude]': photo.gps.GPSLatitude.toString(),
                            'GPS[GPSLongitude]': photo.gps.GPSLongitude.toString(),
                            'employee_name': photo.userName
                        }
                    }
                ).then(response => {
                    if (response.status == 201) {
                        dispatch(MainsObjectAction.deleteMainsObjectData(photo.photoName));
                        dispatch(MainsObjectAction.removeMainsObjectFileAction(photo.uri));
                    } else {
                        setError(true);
                    }
                    let percent = Math.floor((++counter / count) * 100);
                    setProgress({
                        percent: percent,
                        text: percent.toString() + '%'
                    });
                    if (percent >= 100) {
                        setShowProgBar(false);
                    }
                });
            });
        }
    };

    useLayoutEffect(() => {
        let navigationOptions: StackNavigationOptions = {
            headerTitle: 'Фотофиксация',
            headerStyle: {
                backgroundColor: THEME.BACKGROUND_COLOR,
            },
            headerTitleStyle: {
                color: THEME.SECONDARY_COLOR,
                textAlign: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
            },
            headerLeft: () => (<View/>),
            headerRight: () => (<View/>),
        };
        navigation.setOptions(navigationOptions);
    }, [navigation]);

    useEffect(() => {
        setPhotos(getMainObjectsPhotos(mainsObjects));
    }, [mainsObjects]);

    useEffect(() => {
        (async () => {
            let locIsEnabled = await Location.hasServicesEnabledAsync();
            if (!locIsEnabled) {
                Alert.alert('Пожалуйста включите геолокацию');
            }
        })();
    }, []);

    return (
        <View style={styles.wrapper}>
            <View style={styles.header}>
                <Text style={styles.title}>Сетевые объекты</Text>
                <Text style={styles.text}>Сфотографируйте сетевой объект и отправьте фото оператору</Text>
            </View>

            {showProgBar &&
            <View style={{paddingHorizontal: 10,}}>
                <ProgressBar
                    progress={progress.percent}
                    text={progress.text}
                />
            </View>
            }
            <View style={styles.content}>

                {error &&
                <Text>Произошла ошибка. Попробуйте позже или обратитесь к администратору.</Text>
                }

                {photos?.length > 0 ? (
                    <>
                        <FlatList
                            data={photos}
                            contentContainerStyle={{justifyContent: 'center', alignItems: 'center'}}
                            horizontal={false}
                            numColumns={2}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={value => {
                                return (
                                    <TouchableOpacity
                                        style={styles.imageContainer}
                                        onPress={() => setShowModal({status: true, index: value.index})}
                                    >
                                        <Image
                                            resizeMode={'contain'}
                                            style={{...styles.image, width: screenSize.width * 0.4,}}
                                            source={{uri: value.item}}
                                        />
                                        <View style={styles.iconContainer}>
                                            <FontAwesome name="search-plus" size={34} color={THEME.BACKGROUND_WITH_OPACITY}/>
                                            {/*<Ionicons name="search" size={28} color={THEME.BACKGROUND_WITH_OPACITY}/>*/}
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                        />
                        <Modal
                            visible={showModal.status}
                            transparent={true}
                            onRequestClose={() => setShowModal({status: false, index: 0})}
                        >
                            <ImageViewer
                                onClick={() => {}}
                                enableSwipeDown={true}
                                onSwipeDown={() => setShowModal({status: false, index: 0})}
                                imageUrls={convertPhotos(photos)}
                                index={showModal.index}
                            />
                        </Modal>
                    </>
                ) : (
                    <Card style={styles.noPhotoCard}>
                        <Text style={styles.noPhotoText}>Нет фотографий</Text>
                    </Card>
                )}
            </View>

            {/*<Modal*/}
            {/*    style={styles.modal}*/}
            {/*    visible={showModal}*/}
            {/*    backdropStyle={styles.backdrop}*/}
            {/*    onBackdropPress={onCloseModal}*/}
            {/*>*/}
            {/*    <TouchableOpacity style={{width: '100%', height: '100%',}} onPress={onCloseModal}>*/}
            {/*        <Image resizeMode={'contain'} style={{width: '100%', height: '100%',}} source={{uri: imageUrl}}/>*/}
            {/*    </TouchableOpacity>*/}
            {/*</Modal>*/}

            <View style={styles.btnsGroup}>
                <PhotoPicker
                    onMakePhoto={savePhoto}
                    withoutImage={true}
                    isNeedLocation={true}
                    buttonStyleStatus="primary"
                    buttonAppearance={'outline'}
                    buttonIconColor={THEME.PRIMARY_COLOR}
                    buttonActiveOpacity={0.7}
                    styleBtn={styles.outlineBtn}
                />
                <Button
                    style={{marginBottom: 10}}
                    status={'primary'}
                    size={'medium'}
                    onPress={onSubmitPhotos}
                >
                    Отправить фотографии
                </Button>
            </View>
            <Footer activeBtn={'Photo'}/>
        </View>
    );
}