import {StyleSheet} from "react-native";
import {THEME} from "../../theme";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: THEME.BACKGROUND_COLOR,
    },
    header: {
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    title: {
        fontWeight: '500',
        fontSize: 16,
        marginBottom: 7,
        color: THEME.TEXT_COLOR,
    },
    text: {
        fontSize: 15,
        color: THEME.TEXT_COLOR,
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        width: '100%',
        flexDirection: 'row',
    },
    imageContainer: {
        margin: 5,
        backgroundColor: THEME.BLACK_BACKGROUND,
        borderRadius: 6,
    },
    image: {
        height: 150,
        // marginBottom: 10,
        // width: '100%',
        borderRadius: 6,
    },
    iconContainer: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
        borderRadius: 6,
    },
    outlineBtn: {
        backgroundColor: THEME.BACKGROUND_COLOR,
        color: THEME.PRIMARY_COLOR,
    },
    btnsGroup: {
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    noPhotoCard: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    noPhotoText: {
        color: THEME.OUTLINE_COLOR,
        fontSize: 16,
        fontWeight: '700',
    },
    modal: {
        paddingVertical: 40,
        paddingHorizontal: 20,
        width: '100%',
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backdrop: {
        backgroundColor: THEME.LOADER_BACKGROUND_COLOR,
    },
});