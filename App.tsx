import React, {useState} from 'react';
import { bootstrap } from './src/bootstrap'
import AppLoading from "expo-app-loading";
import {Provider} from "react-redux";
import AppNavigation from "./src/navigation/AppNavigation";
import Store from "./src/store/store";
import * as eva from '@eva-design/eva';
import {ApplicationProvider} from "@ui-kitten/components";
import { default as theme } from './assets/theme/custom-theme.json';
import {enableScreens} from 'react-native-screens';

enableScreens();

export default function App():any {
  const [isReady, setIsReady] = useState(false);

  if (!isReady) {
    return (
        <AppLoading
            startAsync={bootstrap}
            onFinish={() => setIsReady(true)}
            onError={(err: any) => console.log(err)}
        />
    )
  }

  return (
      <Provider store={Store}>
          <ApplicationProvider {...eva} theme={{...eva.light, ...theme}}>
              <AppNavigation/>
          </ApplicationProvider>
      </Provider>
      );
}

